{{- define "api.labels" -}}
{{- include "common.labels" . }}
app.kubernetes.io/component: api
{{- end }}

{{- define "api.selectorLabels" -}}
{{- include "common.selectorLabels" . }}
app.kubernetes.io/component: api
{{- end }}


{{- define "api.name" }}
{{- include "app.fullname" . }}-api
{{- end }}

{{- define "api.portName" -}}
http
{{- end }}
