import pytest

from app.settings import AppSettings
from lib.settings import get_settings


@pytest.fixture(autouse=True, scope="session")
def _setup_app_settings() -> None:
    settings = get_settings(AppSettings)
    settings.standalone = False
