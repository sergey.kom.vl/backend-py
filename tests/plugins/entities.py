import random
import typing
import uuid

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import Account, AccountProfile
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.quests.models import (
    QuestConditions,
    QuestRewards,
    QuestTemplate,
)
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import QuestId
from lib.tarkov.utils import generate_mongodb_id
from tests.utils import TraderEnum


@pytest.fixture
async def account(session: AsyncSession) -> Account:
    account = Account(
        username=str(uuid.uuid4()),
        session_id=str(uuid.uuid4()),
        game_edition="edge_of_darkness",
    )
    session.add(account)
    await session.flush()
    return account


@pytest.fixture
async def account_profile(
    session: AsyncSession,
    account: Account,
) -> AccountProfile:
    profile = AccountProfile(
        account=account,
        name=str(uuid.uuid4()),
        side=random.choice(typing.get_args(PMCSide)),
    )
    session.add(profile)
    await session.flush()
    return profile


@pytest.fixture
async def profile_context(
    profile_storage: ProfileStorage,
    account_profile: AccountProfile,
) -> ProfileContext:
    dto = ProfileCreateDTO(
        name=str(uuid.uuid4()),
        side="Usec",
        voice_id="5fc1223595572123ae7384a3",
        head_id="5cde96047d6c8b20b577f016",
    )
    await profile_storage.initialize_profile(profile=account_profile, dto=dto)
    return await profile_storage.readonly(profile_id=str(account_profile.id))


@pytest.fixture
async def quest_template() -> QuestTemplate:
    return QuestTemplate(
        id=QuestId(generate_mongodb_id()),
        quest_name="Quest Name",
        name="Name",
        conditions=QuestConditions(
            available_for_finish=[],
            available_for_start=[],
            fail=[],
        ),
        can_show_notifications_in_game=True,
        change_quest_message_text="",
        decline_player_message="",
        description="",
        fail_message_text="",
        image="",
        instant_complete=False,
        is_key=True,
        location="bigmap",
        note="",
        restartable=False,
        rewards=QuestRewards(
            fail=[],
            started=[],
            success=[],
        ),
        secret_quest=False,
        side="Pmc",
        started_message_text="",
        success_message_text="",
        trader_id=TraderEnum.prapor.value,
        type="Quest Type",
    )


@pytest.fixture
def random_trader(traders: Traders) -> TraderModel:
    return random.choice(list(traders))
