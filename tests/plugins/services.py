import pytest
from aioinject import InjectionContext
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker

from app.core.di._modules.locales import create_locale_service
from app.core.domain.items.services import CustomizationDB
from app.core.domain.locales.services import LocaleService
from app.core.domain.locations.services import LocationService
from app.core.domain.quests.services import QuestsService
from app.core.domain.traders.repositories import TraderRepository
from app.core.domain.traders.tasks import UpdateTraderAssortTask
from app.core.domain.weather.services import WeatherService
from lib.db import DBContext
from lib.tarkov.achievements.db import AchievementDB
from lib.tarkov.config import QuestConfig
from lib.tarkov.db import AssetsPath
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.quests.storage import QuestStorage
from lib.tarkov.trading.trader import Traders


@pytest.fixture
async def db_context(session: AsyncSession) -> DBContext:
    return session


@pytest.fixture
async def locale_service(assets_path: AssetsPath) -> LocaleService:
    return await create_locale_service(assets_path)


@pytest.fixture
async def traders(inject_context: InjectionContext) -> Traders:
    return await inject_context.resolve(Traders)


@pytest.fixture
async def achievement_db(inject_context: InjectionContext) -> AchievementDB:
    return await inject_context.resolve(AchievementDB)


@pytest.fixture
async def customization_db(inject_context: InjectionContext) -> CustomizationDB:
    return await inject_context.resolve(CustomizationDB)


@pytest.fixture
async def profile_storage(inject_context: InjectionContext) -> ProfileStorage:
    return await inject_context.resolve(
        ProfileStorage,  # type: ignore[type-abstract]
    )


@pytest.fixture
async def weather_service(inject_context: InjectionContext) -> WeatherService:
    return await inject_context.resolve(WeatherService)


@pytest.fixture
async def location_service(inject_context: InjectionContext) -> LocationService:
    return await inject_context.resolve(LocationService)


@pytest.fixture
async def quest_storage(inject_context: InjectionContext) -> QuestStorage:
    return await inject_context.resolve(QuestStorage)


@pytest.fixture
async def quest_config(inject_context: InjectionContext) -> QuestConfig:
    return await inject_context.resolve(QuestConfig)


@pytest.fixture
async def quest_service(inject_context: InjectionContext) -> QuestsService:
    return await inject_context.resolve(QuestsService)


@pytest.fixture
def update_traders_assort_task(
    async_sessionmaker: async_sessionmaker[AsyncSession],
    traders: Traders,
) -> UpdateTraderAssortTask:
    return UpdateTraderAssortTask(
        traders=traders,
        session_factory=async_sessionmaker,
    )


@pytest.fixture
async def item_db(inject_context: InjectionContext) -> ItemDB:
    return await inject_context.resolve(ItemDB)


@pytest.fixture
async def trader_repository(session: AsyncSession) -> TraderRepository:
    return TraderRepository(session=session)
