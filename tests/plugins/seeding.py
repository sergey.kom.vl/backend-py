from datetime import datetime

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.domain.traders.tasks import UpdateTraderAssortTask


@pytest.fixture
async def _seed_traders(
    now: datetime,
    session: AsyncSession,
    update_traders_assort_task: UpdateTraderAssortTask,
) -> None:
    await update_traders_assort_task.run_once(now=now, session=session)
