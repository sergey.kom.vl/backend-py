import enum
from collections.abc import Sequence

from lib.tarkov.inventory import Inventory
from lib.tarkov.items.actions import SchemeItem
from lib.tarkov.items.models import Item
from lib.tarkov.items.upd import Upd
from lib.tarkov.trading.models import TraderBarter
from lib.tarkov.utils import generate_mongodb_id


class TraderEnum(enum.StrEnum):
    prapor = "54cb50c76803fa8b248b4571"


def add_items_required_for_trade(
    inventory: Inventory,
    inventory_root: Item,
    scheme: Sequence[TraderBarter],
    slot_id: str = "hideout",
) -> tuple[Sequence[SchemeItem], Sequence[Item]]:
    scheme_items = []
    items = []

    for scheme_item in scheme:
        money = Item(
            id=generate_mongodb_id(),
            template_id=scheme_item.template_id,
            upd=Upd(StackObjectsCount=scheme_item.count),
        )
        scheme_items.append(SchemeItem(id=money.id, count=scheme_item.count))
        items.append(money)
        location = inventory.find_location(
            item=money,
            children=(),
            parent=inventory_root,
            slot_id=slot_id,
        )
        assert location
        inventory.add(
            item=money,
            children=(),
            location=location,
        )

    return scheme_items, items
