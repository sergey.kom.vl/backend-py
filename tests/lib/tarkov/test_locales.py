from pathlib import Path

import pytest

from lib.tarkov.locales import load_locales


@pytest.fixture
def path(assets_path: Path) -> Path:
    return assets_path.joinpath("locales")


async def test_load_locales(path: Path) -> None:
    result = await load_locales(path)
    assert len(result) == len(list(path.joinpath("global").iterdir())) != 0
