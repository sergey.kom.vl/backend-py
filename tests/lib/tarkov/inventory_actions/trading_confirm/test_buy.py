import random
import uuid

import pytest

from app.core.domain.traders.repositories import TraderRepository
from lib.db import DBContext
from lib.tarkov.inventory import Inventory, ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import TradingConfirmHandler
from lib.tarkov.inventory_actions.errors import (
    NotEnoughItemsForTradeError,
    TraderNotFoundError,
)
from lib.tarkov.items.actions import (
    SchemeItem,
    TradingConfirmAction,
)
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.trading.models import CurrencyIds
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import ItemId
from lib.tarkov.utils import generate_mongodb_id
from tests.utils import TraderEnum, add_items_required_for_trade

pytestmark = [
    pytest.mark.usefixtures("_seed_traders"),
]


@pytest.fixture
def handler(
    item_db: ItemDB,
    traders: Traders,
    db_context: DBContext,
    trader_repository: TraderRepository,
) -> TradingConfirmHandler:
    return TradingConfirmHandler(
        item_db=item_db,
        traders=traders,
        db_context=db_context,
        repository=trader_repository,
    )


@pytest.fixture
def trader(traders: Traders) -> TraderModel:
    trader = traders.get(TraderEnum.prapor)
    assert trader
    return trader


@pytest.fixture
def trader_inventory(traders: Traders, trader: TraderModel) -> Inventory:
    return traders.inventory(trader)


@pytest.fixture
def action(trader: TraderModel, trader_inventory: Inventory) -> TradingConfirmAction:
    item_id = ItemId("64d52512c06f9028d80eb39e")
    item = trader_inventory.get(item_id)
    assert item
    return TradingConfirmAction(
        action="TradingConfirm",
        trader_id=trader.base.id,
        item_id=item_id,
        count=1,
        scheme_id=0,
        type="buy_from_trader",
        scheme_items=[],
    )


async def test_not_enough_items(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


async def test_trader_not_found(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    trader_id = str(uuid.uuid4())
    action.trader_id = trader_id
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == TraderNotFoundError(id=trader_id)


async def test_scheme_item_not_found(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    action.scheme_items = [SchemeItem(id=generate_mongodb_id(), count=42)]
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


async def test_item_not_found(
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
) -> None:
    item_id = generate_mongodb_id()
    action.item_id = item_id
    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_not_enough_in_stack(  # noqa: PLR0913
    handler: TradingConfirmHandler,
    action: TradingConfirmAction,
    action_context: ActionContext,
    inventory: Inventory,
    inventory_root: Item,
    trader: TraderModel,
) -> None:
    scheme_items, items = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    action.scheme_items = scheme_items
    for item in items:
        item.upd["StackObjectsCount"] -= 1

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.unwrap_err() == NotEnoughItemsForTradeError()


@pytest.mark.parametrize(
    "item_id",
    [
        ItemId("64d52512c06f9028d80eb39e"),  # AKS-74U
        ItemId("64d52512c06f9028d80eb398"),  # F-1 grenade barter
    ],
)
async def test_ok(  # noqa: PLR0913
    handler: TradingConfirmHandler,
    inventory: Inventory,
    inventory_root: Item,
    action_context: ActionContext,
    action: TradingConfirmAction,
    trader_inventory: Inventory,
    trader: TraderModel,
    profile_context: ProfileContext,
    item_id: ItemId,
) -> None:
    item = trader_inventory.get(item_id)
    assert item
    action.item_id = item.id

    initial_item_count = len(inventory.items)
    assert initial_item_count == 1

    scheme_items, _ = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    action.scheme_items = scheme_items

    scheme_item = scheme_items[0]
    scheme_item_in_inventory = inventory.get(scheme_item.id)
    assert scheme_item_in_inventory

    result = await handler.handle(
        action=action,
        context=action_context,
    )

    assert result.is_ok()
    assert len(inventory.items) == initial_item_count + 1 + len(
        list(trader_inventory.children(item)),
    )
    assert len(scheme_items) == 1
    expected_sales_sum = (
        scheme_item.count
        if scheme_item_in_inventory.template_id
        == CurrencyIds[trader.base.currency].value
        else 0
    )
    assert (
        profile_context.pmc.traders_info[trader.base.id].sales_sum == expected_sales_sum
    )


async def test_partial_money_stack(  # noqa: PLR0913
    handler: TradingConfirmHandler,
    inventory: Inventory,
    inventory_root: Item,
    action_context: ActionContext,
    action: TradingConfirmAction,
    trader_inventory: Inventory,
    trader: TraderModel,
) -> None:
    item = trader_inventory.get(ItemId("64d52512c06f9028d80eb39e"))
    assert item
    action.item_id = item.id

    scheme_items, money_items = add_items_required_for_trade(
        inventory=inventory,
        inventory_root=inventory_root,
        scheme=trader.assort.barter_scheme[action.item_id][action.scheme_id],
    )
    action.scheme_items = scheme_items
    assert len(money_items) == 1
    diff = random.randint(1, 1000)
    money = money_items[0]
    money.upd["StackObjectsCount"] += diff

    result = await handler.handle(
        action=action,
        context=action_context,
    )
    assert result.is_ok()
    assert money.upd["StackObjectsCount"] == diff
    assert inventory.get(money.id) is not None
