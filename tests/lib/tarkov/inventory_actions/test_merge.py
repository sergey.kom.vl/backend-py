import pytest

from lib.tarkov.inventory import (
    IncompatibleItemsError,
    ItemIsNotStackableError,
    ItemNotFoundError,
    StackSizeError,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import MergeHandler
from lib.tarkov.items.actions import MergeAction
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler() -> MergeHandler:
    return MergeHandler()


async def test_ok(
    handler: MergeHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
) -> None:
    result = await handler.handle(
        action=MergeAction(
            item=stackable_source.id,
            with_=stackable_target.id,
            action="Merge",
        ),
        context=action_context,
    )
    assert result.unwrap() is None


async def test_item_not_found(
    handler: MergeHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
) -> None:
    not_found_id = generate_mongodb_id()

    result = await handler.handle(
        action=MergeAction(
            item=not_found_id,
            with_=stackable_target.id,
            action="Merge",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=not_found_id)

    result = await handler.handle(
        action=MergeAction(
            item=stackable_source.id,
            with_=not_found_id,
            action="Merge",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=not_found_id)


async def test_incompatible_items(
    handler: MergeHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
    item_db: ItemDB,
) -> None:
    stackable_source.template_id = item_db.get_by_name("patron_9x19_GT").id
    result = await handler.handle(
        action=MergeAction(
            item=stackable_source.id,
            with_=stackable_target.id,
            action="Merge",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == IncompatibleItemsError(
        ids=[stackable_source.id, stackable_target.id],
    )

    tpl = item_db.get_by_name("weapon_beretta_m9a3_9x19")
    stackable_source.template_id = tpl.id
    stackable_target.template_id = tpl.id
    result = await handler.handle(
        action=MergeAction(
            item=stackable_source.id,
            with_=stackable_target.id,
            action="Merge",
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemIsNotStackableError()


async def test_stack_size_too_big(
    handler: MergeHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
    stackable_max_stack_size: int,
) -> None:
    stackable_source.upd["StackObjectsCount"] = 2
    stackable_target.upd["StackObjectsCount"] = stackable_max_stack_size - 1

    result = await handler.handle(
        action=MergeAction(
            item=stackable_source.id,
            with_=stackable_target.id,
            action="Merge",
        ),
        context=action_context,
    )
    assert isinstance(result.unwrap_err(), StackSizeError)
