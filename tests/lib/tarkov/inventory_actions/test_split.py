import uuid

import pytest

from lib.tarkov.inventory import (
    Inventory,
    ItemIsNotStackableError,
    ItemNotFoundError,
    SlotAlreadyTakenError,
    StackSizeError,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import SplitHandler
from lib.tarkov.items.actions import Location, SplitAction
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.items.upd import Upd
from lib.tarkov.types import ItemId
from lib.tarkov.utils import generate_mongodb_id
from tests.lib.tarkov.conftest import PATRON_9x19_PST_GZH


@pytest.fixture
def handler() -> SplitHandler:
    return SplitHandler()


@pytest.fixture
async def item_in_inventory(
    inventory: Inventory,
    inventory_root: Item,
) -> Item:
    item = Item(
        id=generate_mongodb_id(),
        template_id=PATRON_9x19_PST_GZH,
        upd=Upd(StackObjectsCount=60),
    )
    inventory.add(
        item,
        children=(),
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        ),
    ).unwrap()
    return item


@pytest.fixture
def action(item_in_inventory: Item, inventory_root: Item) -> SplitAction:
    return SplitAction(
        action="Split",
        split_item=item_in_inventory.id,
        new_item=ItemId(str(uuid.uuid4())),
        container=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=1, y=0, rotation=ItemRotation.Horizontal),
        ),
        count=1,
    )


async def test_ok(
    inventory: Inventory,
    handler: SplitHandler,
    action_context: ActionContext,
    action: SplitAction,
) -> None:
    action.count = 30
    (await handler.handle(action=action, context=action_context)).unwrap()
    item = inventory.get(action.new_item)
    assert item
    assert all(
        item not in container
        for container in (
            action_context.changes.items.changed,
            action_context.changes.items.new,
            action_context.changes.items.deleted,
        )
    )


@pytest.mark.parametrize(
    ("item_count", "requested_count"),
    [
        (1, 1),
        (1, 2),
        (60, 60),
        (60, 61),
    ],
)
async def test_stack_size_too_small(  # noqa: PLR0913
    item_in_inventory: Item,
    handler: SplitHandler,
    action_context: ActionContext,
    item_count: int,
    requested_count: int,
    action: SplitAction,
) -> None:
    item_in_inventory.upd["StackObjectsCount"] = item_count
    action.count = requested_count
    result = (await handler.handle(action=action, context=action_context)).unwrap_err()
    assert result == StackSizeError()


async def test_item_is_not_stackable(
    handler: SplitHandler,
    action_context: ActionContext,
    item_db: ItemDB,
    action: SplitAction,
    item_in_inventory: Item,
) -> None:
    tpl = item_db.get_by_name(name="weapon_beretta_m9a3_9x19")
    item_in_inventory.template_id = tpl.id
    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap_err() == ItemIsNotStackableError()


async def test_item_or_container_not_found(
    item_in_inventory: Item,
    handler: SplitHandler,
    action_context: ActionContext,
    action: SplitAction,
) -> None:
    action.split_item = generate_mongodb_id()
    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap_err() == ItemNotFoundError(id=action.split_item)

    action.split_item = item_in_inventory.id
    action.container.id = generate_mongodb_id()

    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap_err() == ItemNotFoundError(id=action.container.id)


async def test_same_location(
    handler: SplitHandler,
    action_context: ActionContext,
    action: SplitAction,
) -> None:
    action.container.location = ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal)
    result = await handler.handle(action=action, context=action_context)
    assert result.unwrap_err() == SlotAlreadyTakenError(slot=(0, 0), state=True)
