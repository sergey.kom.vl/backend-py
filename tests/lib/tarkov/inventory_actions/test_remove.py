import pytest
from result import Ok

from lib.tarkov.inventory import Inventory, ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import RemoveHandler
from lib.tarkov.items.actions import Location, RemoveAction
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler() -> RemoveHandler:
    return RemoveHandler()


def _add_stacked_items(
    depth: int,
    inventory: Inventory,
    root: Item,
    item_db: ItemDB,
) -> list[Item]:
    tpl = item_db.get_by_name("backpack_pilgrim")
    items = [Item(id=generate_mongodb_id(), template_id=tpl.id) for _ in range(depth)]
    parent = root
    for item in items:
        container = "hideout" if parent is root else "main"
        inventory.add(
            item,
            children=(),
            location=Location(
                id=parent.id,
                container=container,
                location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
            ),
        )
        parent = item
    return items


@pytest.mark.parametrize(
    "depth",
    range(1, 10),
)
async def test_ok(  # noqa: PLR0913
    handler: RemoveHandler,
    action_context: ActionContext,
    inventory: Inventory,
    inventory_root: Item,
    item_db: ItemDB,
    depth: int,
) -> None:
    items = _add_stacked_items(
        depth=depth,
        inventory=inventory,
        root=inventory_root,
        item_db=item_db,
    )
    for item in items:
        assert inventory.get(item.id) is item
    result = await handler.handle(
        action=RemoveAction(action="Remove", item=items[0].id),
        context=action_context,
    )
    assert isinstance(result, Ok)

    for item in items:
        assert inventory.get(item.id) is None


async def test_not_found(
    handler: RemoveHandler,
    action_context: ActionContext,
) -> None:
    item_id = generate_mongodb_id()
    result = await handler.handle(
        action=RemoveAction(action="Remove", item=item_id),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)
