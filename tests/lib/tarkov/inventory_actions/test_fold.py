import pytest

from lib.tarkov.inventory import Inventory, ItemIsNotFoldableError, ItemNotFoundError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import FoldHandler
from lib.tarkov.items.actions import FoldAction
from lib.tarkov.items.models import Item
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler(item_db: ItemDB) -> FoldHandler:
    return FoldHandler(item_db=item_db)


async def test_item_not_found(
    handler: FoldHandler,
    action_context: ActionContext,
) -> None:
    item_id = generate_mongodb_id()
    result = await handler.handle(
        action=FoldAction(
            action="Fold",
            item=item_id,
            value=True,
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)


async def test_cant_fold_non_foldable_items(
    inventory: Inventory,
    inventory_root: Item,
    item_db: ItemDB,
    handler: FoldHandler,
    action_context: ActionContext,
) -> None:
    item = Item(
        id=generate_mongodb_id(),
        template_id=item_db.get_by_name("patron_9x19_PST_gzh").id,
    )
    location = inventory.find_location(
        item=item,
        children=(),
        slot_id="hideout",
        parent=inventory_root,
    )
    assert location
    inventory.add(
        item=item,
        children=(),
        location=location,
    ).unwrap()

    result = await handler.handle(
        action=FoldAction(
            action="Fold",
            item=item.id,
            value=True,
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemIsNotFoldableError(id=item.id)


async def test_ok(
    inventory: Inventory,
    inventory_root: Item,
    item_db: ItemDB,
    handler: FoldHandler,
    action_context: ActionContext,
) -> None:
    item = Item(
        id=generate_mongodb_id(),
        template_id=item_db.get_by_name("stock_toz-106_toz_short_std_wood").id,
    )
    location = inventory.find_location(
        item=item,
        children=(),
        slot_id="hideout",
        parent=inventory_root,
    )
    assert location
    inventory.add(
        item=item,
        children=(),
        location=location,
    ).unwrap()

    result = await handler.handle(
        action=FoldAction(
            action="Fold",
            item=item.id,
            value=True,
        ),
        context=action_context,
    )
    assert result.unwrap() is None
    assert item.upd["Foldable"] == {"Folded": True}
