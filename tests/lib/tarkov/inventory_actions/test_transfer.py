import random

import pytest

from lib.tarkov.inventory import ItemNotFoundError, StackSizeError
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import TransferHandler
from lib.tarkov.items.actions import TransferAction
from lib.tarkov.items.models import Item
from lib.tarkov.utils import generate_mongodb_id


@pytest.fixture
def handler() -> TransferHandler:
    return TransferHandler()


async def test_ok(
    handler: TransferHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
    stackable_max_stack_size: int,
) -> None:
    stackable_source.upd["StackObjectsCount"] = stackable_max_stack_size
    stackable_target.upd["StackObjectsCount"] = random.randint(
        1,
        stackable_max_stack_size - 1,
    )

    to_take = stackable_max_stack_size - stackable_target.upd["StackObjectsCount"]

    result = await handler.handle(
        action=TransferAction(
            action="Transfer",
            count=to_take,
            item=stackable_source.id,
            with_=stackable_target.id,
        ),
        context=action_context,
    )
    result.unwrap()

    assert stackable_target.upd["StackObjectsCount"] == stackable_max_stack_size
    assert (
        stackable_source.upd["StackObjectsCount"] == stackable_max_stack_size - to_take
    )


@pytest.mark.parametrize(
    ("source_count", "target_count_to_full", "count"),
    [
        (10, 15, 15),
        (15, 5, 6),
    ],
)
async def test_stack_size_err(  # noqa: PLR0913
    handler: TransferHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
    stackable_max_stack_size: int,
    source_count: int,
    target_count_to_full: int,
    count: int,
) -> None:
    stackable_source.upd["StackObjectsCount"] = source_count
    stackable_target.upd["StackObjectsCount"] = (
        stackable_max_stack_size - target_count_to_full
    )

    result = await handler.handle(
        action=TransferAction(
            action="Transfer",
            count=count,
            item=stackable_source.id,
            with_=stackable_target.id,
        ),
        context=action_context,
    )
    assert isinstance(result.unwrap_err(), StackSizeError)


async def test_item_not_found(
    handler: TransferHandler,
    action_context: ActionContext,
    stackable_source: Item,
    stackable_target: Item,
) -> None:
    item_id = generate_mongodb_id()

    result = await handler.handle(
        action=TransferAction(
            action="Transfer",
            count=10,
            item=item_id,
            with_=stackable_target.id,
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)

    result = await handler.handle(
        action=TransferAction(
            action="Transfer",
            count=10,
            item=stackable_source.id,
            with_=item_id,
        ),
        context=action_context,
    )
    assert result.unwrap_err() == ItemNotFoundError(id=item_id)
