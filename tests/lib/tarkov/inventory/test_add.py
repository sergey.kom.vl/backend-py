from lib.tarkov.inventory import Inventory
from lib.tarkov.items.actions import Location
from lib.tarkov.items.models import Item, ItemLocation, ItemRotation
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.utils import generate_mongodb_id


async def test_ok(item_db: ItemDB, inventory: Inventory, inventory_root: Item) -> None:
    item = Item(
        id=generate_mongodb_id(),
        template_id=item_db.get_by_name(name="weapon_colt_m4a1_556x45").id,
    )
    children = (
        Item(
            id=generate_mongodb_id(),
            template_id=item_db.get_by_name(name="stock_ar15_adar_wood_v1").id,
            slot_id="mod_pistol_grip",
            parent_id=item.id,
        ),
    )
    result = inventory.add(
        item=item,
        children=children,
        location=Location(
            id=inventory_root.id,
            container="hideout",
            location=ItemLocation(x=0, y=0, rotation=ItemRotation.Horizontal),
        ),
    )
    assert result.unwrap() == (item, children)

    assert inventory.get(item.id) is item
    for child in children:
        assert inventory.get(child.id) is child
