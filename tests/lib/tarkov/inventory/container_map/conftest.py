import pytest

from lib.tarkov.inventory import ContainerMap
from lib.tarkov.items.storage import ItemDB


@pytest.fixture
async def container_map(item_db: ItemDB) -> ContainerMap:
    return ContainerMap(templates=item_db.items)
