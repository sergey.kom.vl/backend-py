from pathlib import Path

from pydantic import BaseModel

from lib.utils import read_pydantic_yaml


class _Model(BaseModel):
    a: int
    b: str
    c: bool


async def test_ok(tmp_path: Path) -> None:
    path = tmp_path.joinpath("file.yaml")
    contents = """
    a: 42
    b: "string"
    c: true
    """
    path.write_text(contents)

    assert await read_pydantic_yaml(path, _Model) == _Model(a=42, b="string", c=True)
