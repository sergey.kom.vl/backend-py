from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from pydantic import TypeAdapter

from app.core.domain.locations.services import LocationService
from lib.tarkov.locations.models import Location


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_locations")


async def test_ok(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    location_service: LocationService,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    adapter = TypeAdapter(dict[str, Location])
    locations = adapter.validate_python(response.json()["data"])
    assert locations == {loc.id_1: loc for loc in location_service.locations}
