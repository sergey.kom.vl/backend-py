from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar
from pydantic import TypeAdapter

from lib.tarkov.customization import Customization
from lib.tarkov.db import AssetsPath
from lib.utils import read_pydantic_json


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_customization")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    assets_path: AssetsPath,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    adapter = TypeAdapter(dict[str, Customization])
    assert adapter.validate_python(response.json()["data"]) == await read_pydantic_json(
        assets_path.joinpath("customization.json"),
        adapter,
    )
