from collections.abc import Sequence

import pytest

from lib.tarkov.db import ResourceDB


@pytest.fixture
def bot_types(resource_db: ResourceDB) -> Sequence[str]:
    return list(resource_db.bots.keys())
