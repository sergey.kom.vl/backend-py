from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.db.models import AccountProfile
from lib.tarkov.customization import Suits


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("trading_customization_storage")


async def test_needs_account(
    user_http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.BAD_REQUEST


async def test_ok(
    user_http_client: httpx.AsyncClient,
    account_profile: AccountProfile,
    endpoint_url: str,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    assert Suits.model_validate(response.json()["data"]) == Suits(
        id=str(account_profile.id),
        suites=[],
    )
