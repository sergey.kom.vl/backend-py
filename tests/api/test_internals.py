from http import HTTPStatus

import httpx
from litestar import Litestar
from sqlalchemy.ext.asyncio import AsyncSession

from app.db.models import Account


async def test_account_does_not_exist(
    user_http_client: httpx.AsyncClient,
    account: Account,
    session: AsyncSession,
    http_app: Litestar,
) -> None:
    await session.delete(account)
    await session.flush()

    response = await user_http_client.post(http_app.route_reverse("profile_list"))
    assert response.status_code == HTTPStatus.UNAUTHORIZED
