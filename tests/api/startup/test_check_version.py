import uuid
from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.startup._schema import CheckVersionSchema


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("check_version")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    client_version = str(uuid.uuid4())
    response = await http_client.post(
        endpoint_url,
        headers={"app-version": f"EFT Client Name {client_version}"},
    )
    assert response.status_code == HTTPStatus.OK
    result = CheckVersionSchema.model_validate(response.json()["data"])
    assert result.is_valid is True
    assert result.latest_version == client_version
