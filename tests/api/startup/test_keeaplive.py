from datetime import datetime
from http import HTTPStatus

import freezegun
import httpx
import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("game_keepalive")


async def test_ok(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    now: datetime,
) -> None:
    with freezegun.freeze_time(now):
        response = await http_client.post(endpoint_url)
        assert response.status_code == HTTPStatus.OK
        assert response.json()["data"] == {
            "utc_time": now.timestamp(),
            "msg": "OK",
        }
