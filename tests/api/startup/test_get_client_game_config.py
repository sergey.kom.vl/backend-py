from datetime import datetime
from http import HTTPStatus

import freezegun
import httpx
import pytest
from _pytest.fixtures import SubRequest
from litestar import Litestar
from pydantic_core import Url

from app.adapters.api.schema import Success
from app.core.domain.locales.services import LocaleService
from app.db.models import Account
from lib.tarkov.backend_info import BackendInfo, ClientGameConfig

pytestmark = [pytest.mark.usefixtures("session")]


@pytest.fixture(params=["client_game_config", "match_group_current"])
def endpoint_url(http_app: Litestar, request: SubRequest) -> str:
    return http_app.route_reverse(request.param)


async def test_unauthorized(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.UNAUTHORIZED


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    locale_service: LocaleService,
    now: datetime,
    account: Account,
) -> None:
    with freezegun.freeze_time(now):
        response = await user_http_client.post(
            endpoint_url,
        )
    assert response.status_code == HTTPStatus.OK

    base_url = str(user_http_client.base_url)
    expected = ClientGameConfig(
        active_profile_id=None,
        account_id=str(account.id),
        languages=locale_service.available_languages,
        total_in_game=0,
        utc_time=int(now.timestamp()),
        backend=BackendInfo(
            lobby=Url(base_url),
            trading=Url(base_url),
            messaging=Url(base_url),
            main=Url(base_url),
            rag_fair=Url(base_url),
        ),
    )
    assert response.json() == Success(data=expected).model_dump(
        by_alias=True,
        mode="json",
    )
