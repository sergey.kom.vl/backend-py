from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

pytestmark = [pytest.mark.usefixtures("session")]


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("profile_list")


async def test_empty(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert response.json()["data"] == []
