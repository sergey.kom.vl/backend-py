from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import Error, Success
from app.db.models import AccountProfile


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("profile_nickname_validate")


async def test_too_short(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    response = await user_http_client.post(endpoint_url, json={"nickname": "aa"})
    assert response.status_code == HTTPStatus.OK
    assert Error.model_validate(response.json()) == Error(
        error_code=256,
        error_message="The nickname is too short",
    )


async def test_taken(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    account_profile: AccountProfile,
) -> None:
    response = await user_http_client.post(
        endpoint_url,
        json={"nickname": account_profile.name},
    )
    assert response.status_code == HTTPStatus.OK
    assert Error.model_validate(response.json()) == Error(
        error_code=255,
        error_message="The nickname is already in use",
    )


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
) -> None:
    name = "Nickname"
    response = await user_http_client.post(endpoint_url, json={"nickname": name})
    assert response.status_code == HTTPStatus.OK
    assert Success.model_validate(response.json()) == Success(data={"status": "ok"})
