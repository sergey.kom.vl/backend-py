from collections.abc import Sequence
from http import HTTPStatus

import httpx
from litestar import Litestar


async def test_ok(
    http_app: Litestar,
    http_client: httpx.AsyncClient,
    locales: Sequence[str],
) -> None:
    for locale in locales:
        url = http_app.route_reverse("locales_menu", locale=locale)
        response = await http_client.post(url)
        assert response.status_code == HTTPStatus.OK
