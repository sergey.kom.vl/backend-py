from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from lib.tarkov.achievements.db import AchievementDB


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("achievement_statistic")


async def test_ok(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    achievement_db: AchievementDB,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    assert response.json()["data"]["elements"] == {
        achievement.id: 0 for achievement in achievement_db.achievements
    }
