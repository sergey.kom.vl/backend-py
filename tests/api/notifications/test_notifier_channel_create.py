from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar

from app.adapters.api.schema import NotificationChannelSchema


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("notification_channel_create")


async def test_ok(
    endpoint_url: str,
    user_http_client: httpx.AsyncClient,
    http_base_url: str,
) -> None:
    response = await user_http_client.post(endpoint_url, json={"uid": ""})
    assert response.status_code == HTTPStatus.OK
    schema = NotificationChannelSchema.model_validate(response.json()["data"])
    assert schema.server == http_base_url.removeprefix("http://") + ":80"
    assert schema.url == ""
