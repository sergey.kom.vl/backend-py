from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("client_builds_list")


async def test_ok(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK

    assert response.json()["data"] == {
        "equipmentBuilds": [],
        "weaponBuilds": [],
        "magazineBuilds": [],
    }
