from http import HTTPStatus

import httpx
import pytest
from litestar import Litestar


@pytest.fixture
def endpoint_url(http_app: Litestar) -> str:
    return http_app.route_reverse("locales_available_languages")


async def test_no_unity_user_agent(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await http_client.post(endpoint_url)
    assert response.status_code == HTTPStatus.OK
    assert "content-encoding" in response.headers


async def test_unity_user_agent(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await http_client.post(
        endpoint_url,
        headers={
            "User-Agent": "UnityPlayer/2021.3.11f1(UnityWebRequest/1.0, libcurl/7.84.0-DEV)",
        },
    )
    assert response.status_code == HTTPStatus.OK
    assert response.headers.get("content-encoding") is None
