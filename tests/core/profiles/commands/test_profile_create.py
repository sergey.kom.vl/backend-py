import random
import typing
import uuid

import pytest
from aioinject import InjectionContext

from app.core.domain.profiles.commands import ProfileCreateCommand
from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import Account
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.abc import ProfileStorage

pytestmark = [pytest.mark.usefixtures("session")]


@pytest.fixture
async def command(inject_context: InjectionContext) -> ProfileCreateCommand:
    return await inject_context.resolve(ProfileCreateCommand)


async def test_ok(
    command: ProfileCreateCommand,
    account: Account,
    profile_storage: ProfileStorage,
) -> None:
    dto = ProfileCreateDTO(
        name=str(uuid.uuid4()),
        side=random.choice(typing.get_args(PMCSide)),
        voice_id=str(uuid.uuid4()),
        head_id=str(uuid.uuid4()),
    )
    result = await command.execute(account=account, dto=dto)
    assert result.name == dto.name
    assert result.side == dto.side

    profile = await profile_storage.readonly(str(result.id))
    assert profile.pmc.info.nickname == dto.name
    assert profile.pmc.info.lower_nickname == str(dto.name)
    assert profile.pmc.info.side == dto.side
    assert profile.pmc.savage is not None
    assert profile.pmc.info.voice == dto.voice_id
    assert profile.pmc.customization.head == dto.head_id
