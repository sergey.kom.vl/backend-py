import pytest
from aioinject import InjectionContext

from app.core.domain.traders.queries import TraderSettingsQuery
from lib.tarkov.trading.trader import Traders


@pytest.fixture
async def query(inject_context: InjectionContext) -> TraderSettingsQuery:
    return await inject_context.resolve(TraderSettingsQuery)


async def test_ok(
    query: TraderSettingsQuery,
    traders: Traders,
) -> None:
    result = await query.execute()
    assert result == sorted([t.base for t in traders], key=lambda t: t.id)
