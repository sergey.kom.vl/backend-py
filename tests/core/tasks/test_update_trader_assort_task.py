from datetime import datetime

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.core.domain.traders.tasks import UpdateTraderAssortTask
from app.db.models import Trader
from lib.tarkov.trading.trader import Traders


async def test_ok(
    update_traders_assort_task: UpdateTraderAssortTask,
    session: AsyncSession,
    now: datetime,
    traders: Traders,
) -> None:
    await update_traders_assort_task.run_once(now=now, session=session)
    traders_in_db = {
        trader.id: trader
        for trader in await session.scalars(
            select(Trader).options(selectinload(Trader.assort)),
        )
    }
    assert len(traders_in_db) == len(traders)
    for trader_model in traders:
        trader = traders_in_db[trader_model.base.id]
        for key in trader_model.assort.barter_scheme:
            assort_item = next(a for a in trader.assort if a.item_id == key)
            item = next(i for i in trader_model.assort.items if i.id == key)
            assert assort_item.count == item.upd["StackObjectsCount"]
