import asyncio

from app import telemetry
from app.core.di import create_container
from app.core.domain.traders.tasks import UpdateTraderAssortTask


async def main() -> None:
    telemetry.setup_telemetry()
    task_classes = (UpdateTraderAssortTask,)
    container = create_container()
    async with (
        container,
        container.context() as ctx,
        asyncio.TaskGroup() as tg,
    ):
        for cls in task_classes:
            task = await ctx.resolve(cls)
            await tg.create_task(task.run_forever())


asyncio.run(main())
