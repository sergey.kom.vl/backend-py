from collections.abc import Mapping
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.locations.queries import LocationsQuery
from lib.tarkov.locations.models import Location


class LocationController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/locations",
        status_code=HTTPStatus.OK,
        name="client_locations",
    )
    @inject
    async def client_locations(
        self,
        query: Annotated[LocationsQuery, Inject],
    ) -> Success[Mapping[str, Location]]:
        return Success(data=await query.execute())
