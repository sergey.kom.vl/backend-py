from collections.abc import Sequence
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.hideout.queries import (
    HideoutAreasQuery,
    HideoutQTEListQuery,
    HideoutRecipesQuery,
    HideoutScavCaseRecipesQuery,
    HideoutSettingsQuery,
)
from lib.tarkov.hideout.models.area import HideoutAreaTemplate
from lib.tarkov.hideout.models.qte import HideoutQuickTimeEvents
from lib.tarkov.hideout.models.recipes import HideoutRecipe, ScavCaseRecipe
from lib.tarkov.hideout.models.settings import HideoutSettings


class HideoutController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/hideout/areas",
        status_code=HTTPStatus.OK,
        name="client_hideout_areas",
    )
    @inject
    async def client_hideout_areas(
        self,
        query: Annotated[HideoutAreasQuery, Inject],
    ) -> Success[Sequence[HideoutAreaTemplate]]:
        return Success(data=await query.execute())

    @post(
        "/client/hideout/qte/list",
        status_code=HTTPStatus.OK,
        name="client_hideout_cte_list",
    )
    @inject
    async def client_hideout_cte_list(
        self,
        query: Annotated[HideoutQTEListQuery, Inject],
    ) -> Success[Sequence[HideoutQuickTimeEvents]]:
        return Success(data=await query.execute())

    @post(
        "/client/hideout/settings",
        status_code=HTTPStatus.OK,
        name="client_hideout_settings",
    )
    @inject
    async def client_hideout_settings(
        self,
        query: Annotated[HideoutSettingsQuery, Inject],
    ) -> Success[HideoutSettings]:
        return Success(data=await query.execute())

    @post(
        "/client/hideout/production/recipes",
        status_code=HTTPStatus.OK,
        name="client_hideout_production_recipes",
    )
    @inject
    async def client_hideout_production_recipes(
        self,
        query: Annotated[HideoutRecipesQuery, Inject],
    ) -> Success[Sequence[HideoutRecipe]]:
        return Success(data=await query.execute())

    @post(
        "/client/hideout/production/scavcase/recipes",
        status_code=HTTPStatus.OK,
        name="client_hideout_production_scavcase_recipes",
    )
    @inject
    async def client_hideout_production_scavcase_recipes(
        self,
        query: Annotated[HideoutScavCaseRecipesQuery, Inject],
    ) -> Success[Sequence[ScavCaseRecipe]]:
        return Success(data=await query.execute())
