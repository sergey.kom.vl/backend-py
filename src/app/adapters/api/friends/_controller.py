from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.friends.queries import FriendListQuery
from lib.tarkov.friends.models import FriendList


class FriendsController(Controller):
    path = "/client/friend"
    middleware = (ZLibMiddleware,)

    @post(
        "/list",
        status_code=HTTPStatus.OK,
        name="friend_list",
    )
    @inject
    async def friend_list(
        self,
        query: Annotated[FriendListQuery, Inject],
    ) -> Success[FriendList]:
        return Success(data=await query.execute())

    @post(
        "/request/list/outbox",
        status_code=HTTPStatus.OK,
        name="friend_request_outbox",
    )
    async def friend_request_outbox(self) -> Success[list[None]]:
        return Success(data=[])

    @post(
        "/request/list/inbox",
        status_code=HTTPStatus.OK,
        name="friend_request_inbox",
    )
    async def friend_request_inbox(self) -> Success[list[None]]:
        return Success(data=[])
