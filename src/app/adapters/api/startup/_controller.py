from collections.abc import Sequence
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, Request, Response, get, post
from litestar.datastructures import Cookie, State
from litestar.exceptions import HTTPException
from litestar.params import Parameter

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.adapters.api.startup._schema import (
    BotDifficultyRequestSchema,
    BotDifficultySchema,
    BrandNameResponseSchema,
    CheckVersionSchema,
    GameStartResponseSchema,
    KeepaliveSchema,
    ServerSchema,
    WeatherDetailsSchema,
    WeatherSchema,
)
from app.adapters.api.ws import get_port
from app.core.domain.startup.queries import (
    ClientGameConfigQuery,
    ClientSettingsQuery,
    GetGlobalsQuery,
)
from app.core.domain.weather.queries import WeatherQuery
from app.db.models import Account
from lib.tarkov.backend_info import ClientGameConfig
from lib.tarkov.db import Globals, ResourceDB
from lib.utils import utc_now


class StartupController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/sp/config/bots/difficulty",
        name="sp_config_bot_difficulty",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def sp_config_bot_difficulty(
        self,
        data: BotDifficultyRequestSchema,
        resource_db: Annotated[ResourceDB, Inject],
    ) -> BotDifficultySchema:
        config = resource_db.bots.get(data.name.lower())
        if config is None:
            raise HTTPException(status_code=HTTPStatus.NOT_FOUND)

        return BotDifficultySchema(
            easy=config.difficulties["easy"],
            normal=config.difficulties["normal"],
            hard=config.difficulties["hard"],
            impossible=config.difficulties["impossible"],
        )

    #
    @get("/getBundleList", name="bundle_list")
    async def bundle_list(self) -> Sequence[None]:
        return []

    @get("/getBrandName", name="brand_name")
    async def brand_name(self) -> BrandNameResponseSchema:
        return BrandNameResponseSchema(
            name="Name",
        )

    @post(
        "/client/game/start",
        name="client_game_start",
        status_code=HTTPStatus.OK,
    )
    async def client_game_start(
        self,
        session_id: Annotated[str, Parameter(cookie="PHPSESSID")],
    ) -> Response[Success[GameStartResponseSchema]]:
        return Response(
            Success(
                data=GameStartResponseSchema(
                    utc_time=int(utc_now().timestamp()),
                ),
            ),
            cookies=[Cookie(key="PHPSESSID", value=session_id)],
        )

    @post(
        "/client/game/version/validate",
        name="client_version_validate",
        status_code=HTTPStatus.OK,
    )
    async def client_version_validate(self) -> Success[None]:
        return Success(data=None)

    @post(
        "/client/game/config",
        name="client_game_config",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def client_game_config(
        self,
        query: Annotated[ClientGameConfigQuery, Inject],
        account: Account,
        request: Request[None, None, State],
    ) -> Success[ClientGameConfig]:
        config = await query.execute(account=account, base_url=str(request.base_url))
        return Success(data=config)

    @post(
        "/client/globals",
        name="client_globals",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def client_globals(
        self,
        query: Annotated[GetGlobalsQuery, Inject],
    ) -> Success[Globals]:
        return Success(data=await query.execute())

    @post(
        "/client/settings",
        name="client_settings",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def client_settings(
        self,
        query: Annotated[ClientSettingsQuery, Inject],
    ) -> Success[object]:
        return Success(data=await query.execute())

    @post("/client/weather", name="client_weather", status_code=HTTPStatus.OK)
    @inject
    async def client_weather(
        self,
        query: Annotated[WeatherQuery, Inject],
    ) -> Success[WeatherSchema]:
        result = await query.execute()
        data = WeatherSchema(
            acceleration=result.acceleration,
            date=result.date,
            time=result.time,
            weather=WeatherDetailsSchema(
                cloud=result.weather.cloud,
                wind_speed=result.weather.wind_speed,
                wind_direction=result.weather.wind_direction,
                wind_gustiness=result.weather.wind_gustiness,
                rain=result.weather.rain,
                rain_intensity=result.weather.rain_intensity,
                fog=result.weather.fog,
                temp=result.weather.temp,
                pressure=result.weather.pressure,
                date=result.weather.date,
                time=result.weather.time,
            ),
        )
        return Success(data=data)

    @post("/client/server/list", name="server_list", status_code=HTTPStatus.OK)
    @inject
    async def server_list(
        self,
        request: Request[None, None, State],
    ) -> Success[Sequence[ServerSchema]]:
        result = [
            ServerSchema(
                ip=request.base_url.hostname,  # type: ignore[arg-type]
                port=get_port(request.base_url.port, scheme=request.base_url.scheme),
            ),
        ]
        return Success(data=result)

    @post(
        "/client/match/group/current",
        name="match_group_current",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def match_group_current(
        self,
        query: Annotated[ClientGameConfigQuery, Inject],
        request: Request[None, None, State],
        account: Account,
    ) -> Success[ClientGameConfig]:
        return Success(
            data=await query.execute(account=account, base_url=str(request.base_url)),
        )

    @post(
        "/client/checkVersion",
        name="check_version",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def check_version(
        self,
        request: Request[None, None, State],
    ) -> Success[CheckVersionSchema]:
        data = CheckVersionSchema(
            is_valid=True,
            latest_version=request.headers.get("app-version", "").removeprefix(
                "EFT Client Name ",
            ),
        )
        return Success(data=data)

    @post("/client/game/keepalive", name="game_keepalive", status_code=HTTPStatus.OK)
    async def game_keepalive(self) -> Success[KeepaliveSchema]:
        return Success(
            data=KeepaliveSchema(
                utc_time=utc_now(),
                msg="OK",
            ),
        )
