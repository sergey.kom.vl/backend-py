from typing import Literal

from pydantic import BaseModel, ConfigDict

from lib.models import CamelCaseModel, DatetimeFloat, PascalCaseModel
from lib.tarkov.bots.models import BotDifficulty


class BotDifficultyRequestSchema(BaseModel):
    name: str


class BotDifficultySchema(PascalCaseModel):
    easy: BotDifficulty
    normal: BotDifficulty
    hard: BotDifficulty
    impossible: BotDifficulty


class BrandNameResponseSchema(BaseModel):
    name: str


class GameStartResponseSchema(BaseModel):
    utc_time: int


class WeatherDetailsSchema(BaseModel):
    cloud: float
    wind_speed: float
    wind_direction: int
    wind_gustiness: float
    rain: int
    rain_intensity: float
    fog: float
    temp: int
    pressure: int
    date: str
    time: str


class WeatherSchema(BaseModel):
    acceleration: int
    time: str
    date: str
    weather: WeatherDetailsSchema


class ServerSchema(BaseModel):
    ip: str
    port: int


class CheckVersionSchema(CamelCaseModel):
    is_valid: bool
    latest_version: str


class KeepaliveSchema(BaseModel):
    model_config = ConfigDict()
    utc_time: DatetimeFloat
    msg: Literal["OK"]
