from lib.models import CamelCaseModel, PascalCaseModel


class BSGLoggingSchema(CamelCaseModel):
    verbosity: int
    send_to_server: bool = False


class AKIVersionSchema(PascalCaseModel):
    version: str


class AKIReleaseInfo(CamelCaseModel):
    beta_disclaimer_text: str | None
    beta_disclaimer_accept_text: str
    server_mods_loaded_text: str
    server_mods_loaded_debug_text: str
    client_mods_loaded_text: str
    client_mods_loaded_debug_text: str
    illegal_plugins_loaded_text: str
    illegal_plugins_exception_text: str
    release_summary_text: str | None
    is_beta: bool | None
    is_moddable: bool | None
    is_modded: bool
    beta_disclaimer_timeout_delay: int
