from litestar import Controller, get, post

from app.adapters.api.aki_compat.schema import (
    AKIReleaseInfo,
    AKIVersionSchema,
    BSGLoggingSchema,
)
from app.adapters.api.middleware import ZLibMiddleware


class AkiCompatabilityController(Controller):  # pragma: no cover
    middleware = (ZLibMiddleware,)

    @get("/singleplayer/bundles")
    async def bundles(self) -> list[None]:
        return []

    @get("/singleplayer/release")
    async def release(self) -> AKIReleaseInfo:
        return AKIReleaseInfo(
            beta_disclaimer_timeout_delay=30,
            beta_disclaimer_accept_text="",
            beta_disclaimer_text="",
            client_mods_loaded_debug_text="",
            client_mods_loaded_text="",
            illegal_plugins_exception_text="",
            illegal_plugins_loaded_text="",
            server_mods_loaded_debug_text="",
            server_mods_loaded_text="",
            release_summary_text="",
            is_beta=False,
            is_moddable=True,
            is_modded=False,
        )

    @get("/singleplayer/settings/version")
    async def version(self) -> AKIVersionSchema:
        return AKIVersionSchema(
            version="Game Version",
        )

    @get("/singleplayer/enableBSGlogging")
    async def bsg_logging(self) -> BSGLoggingSchema:
        return BSGLoggingSchema(verbosity=2)

    @post("/singleplayer/log")
    async def log(self) -> None:
        return None
