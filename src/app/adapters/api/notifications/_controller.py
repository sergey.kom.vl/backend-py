from http import HTTPStatus

from litestar import Controller, Request, post, websocket_listener
from litestar.datastructures import State

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import NotificationChannelSchema, Success
from app.adapters.api.ws import get_ws_channel
from app.db.models import Account


class NotificationController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/notifier/channel/create",
        status_code=HTTPStatus.OK,
        name="notification_channel_create",
    )
    async def notification_channel_create(
        self,
        request: Request[None, None, State],
        account: Account,
    ) -> Success[NotificationChannelSchema]:
        return Success(data=get_ws_channel(request=request, account=account))

    @websocket_listener(
        "/ws/{session_id:str}",
        name="notifications_wss",
    )  # pragma: no cover
    async def ws(
        self,
        session_id: str,
        data: str,  # noqa: ARG002
    ) -> str:  #  pragma: no cover
        return session_id
