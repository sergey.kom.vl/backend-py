from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.presets.queries import UserBuildsQuery
from lib.tarkov.presets.models import UserBuilds


class PresetsController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/builds/list",
        status_code=HTTPStatus.OK,
        name="client_builds_list",
    )
    @inject
    async def client_build_list(
        self,
        query: Annotated[UserBuildsQuery, Inject],
    ) -> Success[UserBuilds]:
        return Success(data=await query.execute())
