from pydantic import BaseModel


class MenuLocaleResponseSchema(BaseModel):
    menu: dict[str, str]
