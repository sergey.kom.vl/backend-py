from collections.abc import Mapping
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.locales._schema import MenuLocaleResponseSchema
from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.locales.queries import (
    AvailableLocalesQuery,
    ClientLocaleQuery,
    MenuLocaleQuery,
)


class LocalesController(Controller):
    path = "/"
    middleware = (ZLibMiddleware,)

    @post(
        "/client/menu/locale/{locale:str}",
        name="locales_menu",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def locales_menu(
        self,
        locale: str,
        query: Annotated[MenuLocaleQuery, Inject],
    ) -> Success[MenuLocaleResponseSchema]:
        return Success(
            data=MenuLocaleResponseSchema(menu=await query.execute(locale=locale)),
        )

    @post(
        "/client/languages",
        name="locales_available_languages",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def locales_available_languages(
        self,
        query: Annotated[AvailableLocalesQuery, Inject],
    ) -> Success[Mapping[str, str]]:
        return Success(data=await query.execute())

    @post(
        "/client/locale/{locale:str}",
        name="locales_main",
        status_code=HTTPStatus.OK,
    )
    @inject
    async def locales_main(
        self,
        locale: str,
        query: Annotated[ClientLocaleQuery, Inject],
    ) -> Success[dict[str, str]]:
        return Success(data=await query.execute(locale=locale))
