from collections.abc import Mapping, Sequence
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, post

from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Elements, Success
from app.core.domain.achievements.queries import (
    AchievementListQuery,
    AchievementStatisticQuery,
)
from lib.tarkov.achievements.models import Achievement


class AchievementsController(Controller):
    middleware = (ZLibMiddleware,)

    @post(
        "/client/achievement/list",
        status_code=HTTPStatus.OK,
        name="achievement_list",
    )
    @inject
    async def list(
        self,
        query: Annotated[AchievementListQuery, Inject],
    ) -> Success[Elements[Sequence[Achievement]]]:
        return Success(data=Elements(elements=await query.execute()))

    @post(
        "/client/achievement/statistic",
        status_code=HTTPStatus.OK,
        name="achievement_statistic",
    )
    @inject
    async def statistic(
        self,
        query: Annotated[AchievementStatisticQuery, Inject],
    ) -> Success[Elements[Mapping[str, float]]]:
        return Success(data=Elements(elements=await query.execute()))
