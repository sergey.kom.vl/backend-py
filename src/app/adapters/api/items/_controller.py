import zlib
from collections.abc import Mapping
from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.litestar import inject
from litestar import Controller, Response, post
from litestar.openapi import ResponseSpec
from pydantic import BaseModel

from app.adapters.api.items._schema import (
    ItemsMovingRequestSchema,
    ItemsMovingResponseSchema,
)
from app.adapters.api.middleware import ZLibMiddleware
from app.adapters.api.schema import Success
from app.core.domain.customization.query import AllCustomizationQuery
from app.core.domain.inventory.commands import InventoryOperationsCommand
from app.core.domain.items.queries import (
    GetClientItemsQuery,
    HandbookQuery,
)
from app.db.models import AccountProfile
from lib.tarkov.customization import Customization
from lib.tarkov.handbook import Handbook
from lib.tarkov.items.models import ItemNode, ItemTemplate
from lib.tarkov.types import ItemTemplateId

_ItemsResponse = Success[Mapping[ItemTemplateId, ItemNode | ItemTemplate]]
_CustomizationResponse = Success[Mapping[str, Customization]]


class ItemsController(Controller):
    path = "/"

    @post(
        "/client/items",
        name="client_items",
        status_code=HTTPStatus.OK,
        cache=True,
        responses={
            HTTPStatus.OK: ResponseSpec(_ItemsResponse),
        },
    )
    @inject
    async def client_items(
        self,
        query: Annotated[GetClientItemsQuery, Inject],
    ) -> Response[bytes]:
        data = _ItemsResponse(data=await query.execute())
        return Response(
            zlib.compress(
                data.model_dump_json(by_alias=True, exclude_defaults=True).encode(),
                level=5,
            ),
            headers={"content-encoding": "deflate"},
        )

    @post(
        "/client/customization",
        name="client_customization",
        status_code=HTTPStatus.OK,
        middleware=[ZLibMiddleware],
    )
    @inject
    async def client_customization(
        self,
        query: Annotated[AllCustomizationQuery, Inject],
    ) -> _CustomizationResponse:
        return _CustomizationResponse(data=await query.execute())

    @post(
        "/client/handbook/templates",
        name="client_handbook_templates",
        status_code=HTTPStatus.OK,
        middleware=[ZLibMiddleware],
    )
    @inject
    async def client_handbook_templates(
        self,
        query: Annotated[HandbookQuery, Inject],
    ) -> Success[Handbook]:
        return Success(data=await query.execute())

    @post(
        "/client/game/profile/items/moving",
        name="profile_items_moving",
        status_code=HTTPStatus.OK,
        middleware=[ZLibMiddleware],
        type_encoders={
            BaseModel: lambda model: model.model_dump(
                mode="json",
                by_alias=True,
                exclude_none=True,
            ),
        },
    )
    @inject
    async def profile_items_moving(
        self,
        data: ItemsMovingRequestSchema,
        account_profile: AccountProfile,
        command: Annotated[InventoryOperationsCommand, Inject],
    ) -> Success[ItemsMovingResponseSchema]:
        result = await command.execute(
            account_profile=account_profile,
            actions=data.data,
        )
        return Success(
            data=ItemsMovingResponseSchema(
                profile_changes=result.changes,
                warnings=result.warnings,
            ),
        )
