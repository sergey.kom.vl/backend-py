from collections.abc import Mapping, Sequence

from lib.models import CamelCaseModel, StrictModel
from lib.tarkov.items.actions import AnyAction, ProfileChanges, WarningModel


class ItemsMovingRequestSchema(StrictModel):
    data: list[AnyAction]
    tm: int
    reload: int


class ItemsMovingResponseSchema(CamelCaseModel):
    profile_changes: Mapping[str, ProfileChanges]
    warnings: Sequence[WarningModel]
