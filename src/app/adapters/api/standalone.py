import asyncio
import contextlib
from collections.abc import AsyncIterator

from app.core.di import create_container
from app.core.domain.traders.tasks import UpdateTraderAssortTask


@contextlib.asynccontextmanager
async def tasks_lifespan(_: object) -> AsyncIterator[None]:  # pragma: no cover
    container = create_container()
    tasks = []
    async with container.context() as ctx:
        task = await ctx.resolve(UpdateTraderAssortTask)
        tasks.append(asyncio.create_task(task.run_forever()))
        yield
