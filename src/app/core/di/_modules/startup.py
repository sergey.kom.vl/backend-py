import aioinject

from app.core.domain.startup.queries import (
    ClientGameConfigQuery,
    ClientSettingsQuery,
    GetGlobalsQuery,
)
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(ClientGameConfigQuery),
    aioinject.Scoped(GetGlobalsQuery),
    aioinject.Scoped(ClientSettingsQuery),
]
