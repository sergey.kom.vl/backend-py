import aioinject

from app.core.domain.locations.queries import LocationsQuery
from app.core.domain.locations.services import LocationService, read_locations
from lib.tarkov.db import AssetsPath
from lib.types import Providers


async def create_location_service(asset_path: AssetsPath) -> LocationService:
    return LocationService(
        locations=await read_locations(asset_path.joinpath("locations")),
    )


providers: Providers = [
    aioinject.Singleton(create_location_service),
    aioinject.Scoped(LocationsQuery),
]
