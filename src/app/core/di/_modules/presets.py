import aioinject

from app.core.domain.presets.queries import UserBuildsQuery
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(UserBuildsQuery),
]
