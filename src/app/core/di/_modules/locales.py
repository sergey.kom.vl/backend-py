import aioinject

from app.core.domain.locales.queries import (
    AvailableLocalesQuery,
    ClientLocaleQuery,
    MenuLocaleQuery,
)
from app.core.domain.locales.services import LocaleService
from lib.tarkov.db import AssetsPath
from lib.tarkov.locales import load_locales
from lib.types import Providers


async def create_locale_service(path: AssetsPath) -> LocaleService:
    locales = await load_locales(path.joinpath("locales"))
    return LocaleService(
        locales=locales,
    )


providers: Providers = [
    aioinject.Singleton(create_locale_service),
    aioinject.Scoped(AvailableLocalesQuery),
    aioinject.Scoped(MenuLocaleQuery),
    aioinject.Scoped(ClientLocaleQuery),
]
