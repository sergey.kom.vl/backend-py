import aioinject

from app.core.domain.weather.queries import WeatherQuery
from app.core.domain.weather.services import WeatherService
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(WeatherQuery),
    aioinject.Singleton(WeatherService),
]
