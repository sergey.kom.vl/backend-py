import aioinject

from app.core.domain.friends.queries import FriendListQuery
from lib.types import Providers

providers: Providers = [
    aioinject.Scoped(FriendListQuery),
]
