import aioinject
from pydantic import TypeAdapter

from app.core.domain.items.queries import GetClientItemsQuery, HandbookQuery
from app.core.domain.items.services import CustomizationDB, HandbookDB
from lib.tarkov.customization import Customization
from lib.tarkov.db import AssetsPath
from lib.tarkov.handbook import Handbook
from lib.tarkov.items.models import ItemNode, ItemTemplate
from lib.tarkov.items.storage import ItemDB
from lib.types import Providers
from lib.utils import read_json, read_pydantic_json


async def create_items_db(path: AssetsPath) -> ItemDB:
    data = await read_json(path.joinpath("items.json"))
    nodes = [item for item in data.values() if item["_type"] == "Node"]
    nodes_adapter = TypeAdapter(list[ItemNode])

    items = [item for item in data.values() if item["_type"] == "Item"]
    items_adapter = TypeAdapter(list[ItemTemplate])
    return ItemDB(
        items={item.id: item for item in items_adapter.validate_python(items)},
        nodes={node.id: node for node in nodes_adapter.validate_python(nodes)},
    )


async def create_customization_db(path: AssetsPath) -> CustomizationDB:
    return CustomizationDB(
        customization=await read_pydantic_json(
            path.joinpath("customization.json"),
            TypeAdapter(dict[str, Customization]),
        ),
    )


async def create_handbook_db(path: AssetsPath) -> HandbookDB:
    handbook = await read_pydantic_json(
        path.joinpath("handbook.json"),
        Handbook,
    )
    return HandbookDB(
        items=handbook.items,
        categories=handbook.categories,
    )


providers: Providers = [
    aioinject.Singleton(create_items_db),
    aioinject.Singleton(create_customization_db),
    aioinject.Singleton(create_handbook_db),
    aioinject.Scoped(GetClientItemsQuery),
    aioinject.Scoped(HandbookQuery),
]
