import contextlib
import functools
import itertools
from collections.abc import AsyncIterator, Iterable
from pathlib import Path
from typing import Any

import aioinject
from aioinject import Object, Provider, Singleton
from pydantic import BaseModel

from app import db
from lib.plugins.registry import PluginRegistry
from lib.tarkov.config import QuestConfig, StorageConfig, WeatherConfig
from lib.tarkov.db import AssetsPath, create_resource_db
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.paths import asset_dir_path
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.profiles.init import ProfileInitializer
from lib.tarkov.profiles.models import ProfileStoragePath
from lib.tarkov.profiles.storage import FileSystemProfileStorage
from lib.types import PathOrStr
from lib.utils import read_pydantic_yaml

from ._extensions import OnAppInit
from ._modules import (
    accounts,
    achievements,
    customization,
    friends,
    hideout,
    inventory,
    items,
    locales,
    locations,
    presets,
    profiles,
    quests,
    startup,
    traders,
    weather,
)

modules: Iterable[Iterable[Provider[Any]]] = [
    accounts.providers,
    achievements.providers,
    customization.providers,
    db.providers,
    friends.providers,
    hideout.providers,
    inventory.providers,
    items.providers,
    locales.providers,
    locations.providers,
    presets.providers,
    profiles.providers,
    quests.providers,
    startup.providers,
    traders.providers,
    weather.providers,
]
_config_files: Iterable[tuple[PathOrStr, type[BaseModel]]] = [
    ("weather.yaml", WeatherConfig),
    ("storage.yaml", StorageConfig),
    ("quests.yaml", QuestConfig),
]


async def _profile_storage_path() -> ProfileStoragePath:
    return ProfileStoragePath(Path(".data"))  # pragma: no cover


@contextlib.asynccontextmanager
async def _create_profile_storage(
    storage_path: ProfileStoragePath,
    initializer: ProfileInitializer,
    item_db: ItemDB,
    settings: StorageConfig,
    plugins: PluginRegistry,
) -> AsyncIterator[ProfileStorage]:
    async with FileSystemProfileStorage(
        storage_path=storage_path,
        initializer=initializer,
        item_db=item_db,
        settings=settings.filesystem,
        plugins=plugins,
    ).context() as storage:
        yield storage


@functools.cache
def create_container() -> aioinject.Container:
    container = aioinject.Container(extensions=[OnAppInit()])
    container.register(Object(container, type_=aioinject.Container))

    for provider in itertools.chain.from_iterable(modules):
        container.register(provider)

    container.register(Object(asset_dir_path, AssetsPath))
    container.register(Singleton(create_resource_db))
    container.register(Singleton(_profile_storage_path))
    container.register(Singleton(_create_profile_storage))

    for path, model in _config_files:
        container.register(
            Singleton(
                functools.partial(
                    read_pydantic_yaml,
                    path=Path("./config").joinpath(path),
                    model=model,
                ),
                type_=model,
            ),
        )

    container.register(Singleton(PluginRegistry))

    return container
