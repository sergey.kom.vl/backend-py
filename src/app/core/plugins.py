from app.core.domain.profiles.plugins import ProfileFixTradersInfo
from lib.plugins.registry import PluginRegistry


def register_plugins(plugins: PluginRegistry) -> None:
    plugins.register(ProfileFixTradersInfo)
