import dataclasses


@dataclasses.dataclass
class NotFoundError:
    pass
