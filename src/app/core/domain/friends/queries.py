from lib.tarkov.friends.models import FriendList


class FriendListQuery:
    async def execute(self) -> FriendList:
        return FriendList(
            friends=[],
            ignore=[],
            in_ignore_list=[],
        )
