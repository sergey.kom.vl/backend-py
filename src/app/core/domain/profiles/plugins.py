from typing import final

from lib.plugins.profile import OnProfileLoadPlugin
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.models import TraderInfo
from lib.tarkov.trading.trader import Traders


@final
class ProfileFixTradersInfo(OnProfileLoadPlugin):
    def __init__(self, traders: Traders) -> None:
        self._traders = traders

    async def on_profile_load(
        self,
        profile_context: ProfileContext,
    ) -> None:
        for trader in self._traders:
            profile_context.pmc.traders_info.setdefault(
                trader.base.id,
                TraderInfo(disabled=False, standing=0, unlocked=True),
            )
