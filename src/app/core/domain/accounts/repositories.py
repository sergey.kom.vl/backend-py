from collections.abc import Sequence

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import Account
from app.db.models._account import AccountProfile
from lib.tarkov.customization import PMCSide


class AccountRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(
        self,
        session_id: str,
        *,
        options: Sequence[ORMOption] | None = None,
    ) -> Account | None:
        stmt = select(Account).where(Account.session_id == session_id)
        if options:  # pragma: no branch
            stmt = stmt.options(*options)
        return await self._session.scalar(stmt)


class ProfileRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(self, name: str) -> AccountProfile | None:
        stmt = select(AccountProfile).where(AccountProfile.name == name)
        return await self._session.scalar(stmt)

    async def create(
        self,
        account: Account,
        name: str,
        side: PMCSide,
    ) -> AccountProfile:
        profile = AccountProfile(account=account, name=name, side=side)
        self._session.add(profile)
        await self._session.flush()
        return profile
