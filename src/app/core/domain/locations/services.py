import asyncio
from pathlib import Path

from lib.tarkov.locations.models import Location
from lib.utils import read_pydantic_json


async def _read_location(path: Path) -> Location:
    return await read_pydantic_json(path.joinpath("base.json"), Location)


async def read_locations(path: Path) -> list[Location]:
    tasks = []
    for d in path.iterdir():
        if not d.is_dir():
            continue  # pragma: no cover
        tasks.append(_read_location(d))
    return list(await asyncio.gather(*tasks))


class LocationService:
    def __init__(self, locations: list[Location]) -> None:
        self.locations = locations
