from collections.abc import Mapping

from app.core.domain.locations.services import LocationService
from lib.tarkov.locations.models import Location


class LocationsQuery:
    def __init__(self, location_service: LocationService) -> None:
        self._service = location_service

    async def execute(self) -> Mapping[str, Location]:
        return {loc.id_1: loc for loc in self._service.locations}
