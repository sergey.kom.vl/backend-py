import dataclasses


@dataclasses.dataclass
class WeatherDetailsDTO:
    cloud: float
    wind_speed: float
    wind_direction: int
    wind_gustiness: float
    rain: int
    rain_intensity: float
    fog: float
    temp: int
    pressure: int
    date: str
    time: str


@dataclasses.dataclass
class WeatherDTO:
    acceleration: int
    time: str
    date: str
    weather: WeatherDetailsDTO
