from typing import Final

from lib.tarkov.hideout.models.area import HideoutAreaTemplate
from lib.tarkov.hideout.models.qte import HideoutQuickTimeEvents
from lib.tarkov.hideout.models.recipes import HideoutRecipe, ScavCaseRecipe
from lib.tarkov.hideout.models.settings import HideoutSettings


class HideoutService:
    def __init__(  # noqa: PLR0913
        self,
        area_templates: list[HideoutAreaTemplate],
        quick_time_events: list[HideoutQuickTimeEvents],
        settings: HideoutSettings,
        recipes: list[HideoutRecipe],
        scav_case_recipes: list[ScavCaseRecipe],
    ) -> None:
        self.areas: Final = area_templates
        self.quick_time_events: Final = quick_time_events
        self.settings: Final = settings
        self.recipes: Final = recipes
        self.scav_case_recipes: Final = scav_case_recipes
