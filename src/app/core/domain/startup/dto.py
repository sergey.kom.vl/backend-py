import dataclasses
from collections.abc import Mapping


@dataclasses.dataclass(slots=True, frozen=True, kw_only=True)
class ClientConfigDTO:
    languages: Mapping[str, str]
    account_id: str
    active_profile_id: str | None
