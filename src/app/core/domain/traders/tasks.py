import asyncio
import logging
import random
from collections.abc import Sequence
from datetime import UTC, datetime, timedelta

import sentry_sdk
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker

from app.db.models import Trader, TraderPlayerRestriction
from app.db.models._traders import TraderAssort
from lib.tarkov.trading.trader import Traders
from lib.utils import utc_now


class UpdateTraderAssortTask:
    def __init__(
        self,
        traders: Traders,
        session_factory: async_sessionmaker[AsyncSession],
    ) -> None:
        self._traders = traders
        self._session_factory = session_factory

    async def run_forever(self) -> None:  # pragma: no cover
        while True:
            now = utc_now()
            try:
                async with self._session_factory.begin() as session:
                    traders = await self.run_once(
                        now=now,
                        session=session,
                    )
            except Exception as e:
                sentry_sdk.capture_exception(e)
                logging.exception("Error during updating traders assort")
                await asyncio.sleep(5)
            else:
                next_resupply = min(t.next_resupply for t in traders)
                await asyncio.sleep(next_resupply.timestamp() - now.timestamp())

    async def run_once(
        self,
        now: datetime,
        session: AsyncSession,
    ) -> Sequence[Trader]:
        traders = list(await session.scalars(select(Trader)))
        traders.extend(self._init_traders(traders))
        session.add_all(traders)
        for trader in traders:
            if trader.next_resupply > now:
                continue  # pragma: no cover
            delta_seconds = random.randrange(
                int(timedelta(hours=1).total_seconds()),
                int(timedelta(hours=4).total_seconds()),
            )
            trader.next_resupply = now + timedelta(seconds=delta_seconds)
            await self._remove_restrictions(session=session, trader=trader)
            await self._update_items_count(session=session, trader=trader)
        return traders

    async def _remove_restrictions(
        self,
        session: AsyncSession,
        trader: Trader,
    ) -> None:
        stmt = delete(TraderPlayerRestriction).where(
            TraderPlayerRestriction.trader == trader,
        )
        await session.execute(stmt)

    async def _update_items_count(
        self,
        session: AsyncSession,
        trader: Trader,
    ) -> None:
        await session.execute(delete(TraderAssort).where(TraderAssort.trader == trader))
        trader_model = self._traders.get(trader.id)
        if not trader_model:
            raise ValueError  # pragma: no cover

        items = [
            item
            for item in trader_model.assort.items
            if item.id in trader_model.assort.barter_scheme
        ]
        assort = [
            TraderAssort(
                trader=trader,
                item_id=item.id,
                count=item.upd.get("StackObjectsCount", 0),
            )
            for item in items
        ]
        session.add_all(assort)

    def _init_traders(self, traders: list[Trader]) -> Sequence[Trader]:
        existing_ids = {t.id for t in traders}
        return [
            Trader(
                id=trader.base.id,
                next_resupply=datetime.min.replace(tzinfo=UTC),
            )
            for trader in self._traders
            if trader.base.id not in existing_ids
        ]
