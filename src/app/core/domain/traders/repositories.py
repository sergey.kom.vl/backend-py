from collections.abc import Sequence

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import AccountProfile, Trader, TraderPlayerRestriction
from lib.tarkov.types import ItemId


class TraderRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def get(self, id_: str, options: Sequence[ORMOption] = ()) -> Trader | None:
        stmt = select(Trader).where(Trader.id == id_).options(*options)
        return await self._session.scalar(stmt)

    async def restrictions(
        self,
        trader_id: str,
        profile: AccountProfile,
    ) -> Sequence[TraderPlayerRestriction]:
        stmt = select(TraderPlayerRestriction).where(
            TraderPlayerRestriction.trader_id == trader_id,
            TraderPlayerRestriction.profile == profile,
        )
        return (await self._session.scalars(stmt)).all()

    async def restriction(
        self,
        trader_id: str,
        profile: AccountProfile,
        item_id: ItemId,
    ) -> TraderPlayerRestriction | None:
        return await self._session.get(
            TraderPlayerRestriction,
            (trader_id, profile.id, item_id),
            with_for_update=True,
        )
