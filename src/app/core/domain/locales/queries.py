from collections.abc import Mapping

from app.core.domain.locales.services import LocaleService


class _Base:
    def __init__(self, service: LocaleService) -> None:
        self._service = service


class AvailableLocalesQuery(_Base):
    async def execute(self) -> Mapping[str, str]:
        return self._service.available_languages


class MenuLocaleQuery(_Base):
    async def execute(self, locale: str) -> dict[str, str]:
        return self._service.locales[locale].menu


class ClientLocaleQuery(_Base):
    async def execute(self, locale: str) -> dict[str, str]:
        return self._service.locales[locale].locale
