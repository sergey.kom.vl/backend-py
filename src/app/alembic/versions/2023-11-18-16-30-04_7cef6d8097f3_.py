"""
empty message

Revision ID: 7cef6d8097f3
Revises: da9327965db6
Create Date: 2023-11-18 16:30:04.267628

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7cef6d8097f3"
down_revision: str | None = "da9327965db6"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "trader_assort",
        sa.Column("trader_id", sa.String(), nullable=False),
        sa.Column("item_id", sa.String(), nullable=False),
        sa.Column("count", sa.Integer(), nullable=False),
        sa.CheckConstraint("count >= 0", name=op.f("check_count_gte_0")),
        sa.ForeignKeyConstraint(
            ["trader_id"],
            ["trader.id"],
            name=op.f("fk_trader_assort_trader_id_trader"),
        ),
        sa.PrimaryKeyConstraint("trader_id", "item_id", name=op.f("pk_trader_assort")),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("trader_assort")
    # ### end Alembic commands ###
