"""
empty message

Revision ID: 400b5caccf66
Revises: aadd49631bc3
Create Date: 2023-11-11 10:16:35.110623

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "400b5caccf66"
down_revision: str | None = "aadd49631bc3"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("profile", schema=None) as batch_op:
        batch_op.add_column(
            sa.Column(
                "side",
                sa.Enum("Bear", "Usec", native_enum=False),
                nullable=False,
            ),
        )


def downgrade() -> None:
    with op.batch_alter_table("profile", schema=None) as batch_op:
        batch_op.drop_column("side")
