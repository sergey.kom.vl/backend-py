"""
empty message

Revision ID: bf720089770f
Revises: b7673b1cba60
Create Date: 2023-11-10 13:38:00.413815

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bf720089770f"
down_revision: str | None = "b7673b1cba60"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    with op.batch_alter_table("account", schema=None) as batch_op:
        batch_op.add_column(sa.Column("profile_id", sa.String(), nullable=False))
        batch_op.create_unique_constraint(
            batch_op.f("uq_account_profile_id"),
            ["profile_id"],
        )


def downgrade() -> None:
    with op.batch_alter_table("account", schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f("uq_account_profile_id"), type_="unique")
        batch_op.drop_column("profile_id")
