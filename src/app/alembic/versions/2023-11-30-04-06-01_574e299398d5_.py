"""
empty message

Revision ID: 574e299398d5
Revises: 7cef6d8097f3
Create Date: 2023-11-30 04:06:01.265952

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "574e299398d5"
down_revision: str | None = "7cef6d8097f3"
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "trader_player_restriction",
        sa.Column("trader_id", sa.String(), nullable=False),
        sa.Column("profile_id", sa.Uuid(), nullable=False),
        sa.Column("item_id", sa.String(), nullable=False),
        sa.Column("bought_count", sa.Integer(), nullable=False),
        sa.CheckConstraint("bought_count >= 0", name=op.f("check_bought_count_gte_0")),
        sa.ForeignKeyConstraint(
            ["profile_id"],
            ["profile.id"],
            name=op.f("fk_trader_player_restriction_profile_id_profile"),
        ),
        sa.ForeignKeyConstraint(
            ["trader_id"],
            ["trader.id"],
            name=op.f("fk_trader_player_restriction_trader_id_trader"),
        ),
        sa.PrimaryKeyConstraint(
            "trader_id",
            "profile_id",
            "item_id",
            name=op.f("pk_trader_player_restriction"),
        ),
    )


def downgrade() -> None:
    op.drop_table("trader_player_restriction")
