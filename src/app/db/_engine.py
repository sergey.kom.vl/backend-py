from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from app.settings import DatabaseSettings
from lib.settings import get_settings

_settings = get_settings(DatabaseSettings)

_engine_kwargs = (
    {
        "pool_size": 20,
        "pool_pre_ping": True,
        "pool_use_lifo": True,
    }
    if not _settings.is_sqlite
    else {}
)
engine = create_async_engine(
    _settings.connection_string,
    echo=_settings.echo,
    **_engine_kwargs,
)
async_session_factory = async_sessionmaker(bind=engine, expire_on_commit=False)
