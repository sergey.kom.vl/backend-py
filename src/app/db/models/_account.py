from __future__ import annotations

import uuid

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship

from app.db import Base
from lib.tarkov.customization import PMCSide


class Account(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "account"

    id: Mapped[int] = mapped_column(primary_key=True, init=False)
    session_id: Mapped[str] = mapped_column(unique=True)
    username: Mapped[str]
    game_edition: Mapped[str]

    profile: Mapped[AccountProfile | None] = relationship(
        back_populates="account",
        uselist=False,
        default=None,
    )


class AccountProfile(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "profile"

    id: Mapped[uuid.UUID] = mapped_column(
        primary_key=True,
        default_factory=uuid.uuid4,
        init=False,
    )
    account_id: Mapped[int] = mapped_column(
        ForeignKey("account.id"),
        init=False,
        unique=True,
    )
    name: Mapped[str] = mapped_column(unique=True)
    side: Mapped[PMCSide]

    account: Mapped[Account] = relationship(back_populates="profile")
