def scale(value: float, a: float, b: float) -> float:
    if not (0 <= value <= 1):
        msg = "value must be between 0 and 1"
        raise ValueError(msg)
    return a + (b - a) * value
