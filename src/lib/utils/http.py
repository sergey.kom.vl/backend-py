from http import HTTPStatus


def http_is_err(status: int) -> bool:
    return HTTPStatus.BAD_REQUEST <= status <= 599  # noqa: PLR2004
