from typing import TypeVar, assert_never, final

from aioinject import Container, InjectionContext, Scoped
from aioinject.context import context_var

from lib.plugins.profile import OnProfileLoadPlugin, ProfilePlugin
from lib.tarkov.profiles.dto import ProfileContext

Plugin = ProfilePlugin

TPlugin = TypeVar("TPlugin", bound=Plugin)


async def _resolve(plugin: type[TPlugin]) -> TPlugin:  # Dirty hack
    context = context_var.get()
    assert isinstance(context, InjectionContext)  # noqa: S101
    return await context.resolve(plugin)


@final
class ProfilePluginRegistry(ProfilePlugin):
    def __init__(self) -> None:
        self._on_load: list[type[OnProfileLoadPlugin]] = []

    def register(self, plugin: type[ProfilePlugin]) -> None:
        if issubclass(plugin, OnProfileLoadPlugin):
            self._on_load.append(plugin)
        else:
            assert_never(plugin)  # pragma: no cover

    async def on_profile_load(self, context: ProfileContext) -> None:
        for plugin_cls in self._on_load:
            plugin = await _resolve(plugin_cls)
            await plugin.on_profile_load(profile_context=context)


class PluginRegistry:
    def __init__(self, container: Container) -> None:
        self._container = container
        self.profile = ProfilePluginRegistry()

    def register(self, plugin: type[Plugin]) -> None:
        self._container.register(Scoped(plugin))
        if issubclass(plugin, ProfilePlugin):
            self.profile.register(plugin)
        else:
            assert_never(plugin)  # pragma: no cover
