from typing import Protocol, runtime_checkable

from lib.tarkov.profiles.dto import ProfileContext


@runtime_checkable
class OnProfileLoadPlugin(Protocol):
    async def on_profile_load(
        self,
        profile_context: ProfileContext,
    ) -> None: ...


ProfilePlugin = OnProfileLoadPlugin
