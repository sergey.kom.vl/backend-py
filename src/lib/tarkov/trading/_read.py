import asyncio
from collections.abc import Sequence
from pathlib import Path

from lib.utils import read_pydantic_json

from .models import TraderAssortModel, TraderBase, TraderQuestAssort
from .trader import TraderModel


async def _read_trader(path: Path) -> TraderModel:
    base = await read_pydantic_json(path.joinpath("base.json"), TraderBase)
    assort = await read_pydantic_json(path.joinpath("assort.json"), TraderAssortModel)
    quest_assort = await read_pydantic_json(
        path.joinpath("questassort.json"),
        TraderQuestAssort,
    )

    return TraderModel(
        base=base,
        assort=assort,
        quest_assort=quest_assort,
    )


async def read_trader_data(path: Path) -> Sequence[TraderModel]:
    tasks = (_read_trader(p) for p in path.iterdir() if p.is_dir())
    return await asyncio.gather(*tasks)
