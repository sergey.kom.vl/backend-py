import asyncio
import logging
from pathlib import Path

from pydantic import BaseModel, TypeAdapter

from lib.utils import read_pydantic_json


class Locale(BaseModel):
    name: str
    verbose_name: str

    locale: dict[str, str]
    menu: dict[str, str]


_languages_adapter = TypeAdapter(dict[str, str])
_language_adapter = TypeAdapter(dict[str, str])


async def _load_locale(
    base_path: Path,
    name: str,
    verbose_name: str,
) -> Locale:
    try:
        menu = await read_pydantic_json(
            base_path.joinpath("menu", f"{name}.json"),
            _language_adapter,
        )
    except FileNotFoundError:
        logging.warning("Locale %s (%s) is missing menu locales", name, verbose_name)
        menu = {}

    return Locale(
        name=name,
        verbose_name=verbose_name,
        locale=await read_pydantic_json(
            base_path.joinpath("global", f"{name}.json"),
            _language_adapter,
        ),
        menu=menu,
    )


async def load_locales(path: Path) -> dict[str, Locale]:
    languages = await read_pydantic_json(
        path.joinpath("languages.json"),
        _languages_adapter,
    )
    tasks = [
        _load_locale(base_path=path, name=name, verbose_name=verbose_name)
        for name, verbose_name in languages.items()
    ]
    locales = await asyncio.gather(*tasks)
    return {locale.name: locale for locale in locales}
