import enum

import pydantic

from lib.models import PascalCaseModel
from lib.tarkov.customization import PMCSide


class MemberCategory(enum.IntEnum):
    default = 0
    developer = 1
    unique_id = 2
    trader = 4
    group = 8
    system = 16


class DialogueDetails(PascalCaseModel):
    nickname: str
    side: PMCSide
    level: int
    member_category: MemberCategory


class DialogueInfo(PascalCaseModel):
    id: str = pydantic.Field(alias="_id")
    info: DialogueDetails


class FriendList(PascalCaseModel):
    friends: list[DialogueInfo]
    ignore: list[str]
    in_ignore_list: list[str]
