import pydantic

from lib.models import CamelCaseModel, MinMaxValue, PascalCaseModel
from lib.tarkov.hideout.models.requirements import AnyHideoutRequirement
from lib.tarkov.items.types import ItemRarity
from lib.tarkov.profiles.models import HideoutAreaType


class HideoutRecipe(CamelCaseModel):
    id: str = pydantic.Field(alias="_id")
    area_type: HideoutAreaType
    requirements: list[AnyHideoutRequirement]
    production_time: int
    need_fuel_for_all_production_time: bool
    locked: bool
    end_product: str
    continuous: bool
    count: int
    production_limit_count: int
    is_encoded: bool


class ScavCaseRecipe(PascalCaseModel):
    id: str = pydantic.Field(alias="_id")
    production_time: int
    requirements: list[AnyHideoutRequirement]
    end_products: dict[ItemRarity, MinMaxValue[int]]
