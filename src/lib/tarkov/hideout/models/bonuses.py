from enum import StrEnum
from typing import Annotated, Literal

import pydantic
from pydantic import ConfigDict, Discriminator

from lib.models import StrictModel
from lib.utils import snake_to_camel


class HideoutBonusType(StrEnum):
    additional_slots = "AdditionalSlots"
    unlock_armor_repair = "UnlockArmorRepair"
    repair_armor_bonus = "RepairArmorBonus"
    stash_size = "StashSize"
    hydration_regeneration = "HydrationRegeneration"
    energy_regeneration = "EnergyRegeneration"
    health_regeneration = "HealthRegeneration"
    text = "TextBonus"
    debuff_end_delay = "DebuffEndDelay"
    maximum_energy = "MaximumEnergyReserve"
    scav_cooldown = "ScavCooldownTimer"
    quest_money_reward = "QuestMoneyReward"
    insurance_return_time = "InsuranceReturnTime"
    flea_commission = "RagfairCommission"
    experience_rate = "ExperienceRate"
    skill_group_leveling_boost = "SkillGroupLevelingBoost"
    fuel_consumption = "FuelConsumption"
    unlock_weapon_repair = "UnlockWeaponRepair"
    unlock_weapon_modification = "UnlockWeaponModification"
    weapon_repair_bonus = "RepairWeaponBonus"


class _HideoutBonusBase(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    id: str
    value: int
    passive: bool
    production: bool
    visible: bool
    icon: str | None = None


class HideoutBonusAdditionalSlots(_HideoutBonusBase):
    filter_: list[str] = pydantic.Field(alias="filter")
    type: Literal[HideoutBonusType.additional_slots]


class HideoutBonusStashSize(_HideoutBonusBase):
    template_id: str
    type: Literal[HideoutBonusType.stash_size]


class HideoutGenericBonus(_HideoutBonusBase):
    type: Literal[
        HideoutBonusType.hydration_regeneration,
        HideoutBonusType.energy_regeneration,
        HideoutBonusType.health_regeneration,
        HideoutBonusType.maximum_energy,
        HideoutBonusType.repair_armor_bonus,
        HideoutBonusType.unlock_armor_repair,
        HideoutBonusType.scav_cooldown,
        HideoutBonusType.quest_money_reward,
        HideoutBonusType.insurance_return_time,
        HideoutBonusType.flea_commission,
        HideoutBonusType.fuel_consumption,
        HideoutBonusType.unlock_weapon_repair,
        HideoutBonusType.unlock_weapon_modification,
        HideoutBonusType.weapon_repair_bonus,
        HideoutBonusType.text,
        HideoutBonusType.debuff_end_delay,
        HideoutBonusType.experience_rate,
    ]


class HideoutSkillGroupLevelingBonus(_HideoutBonusBase):
    skill_type: str
    type: Literal[HideoutBonusType.skill_group_leveling_boost]


AnyHideoutAreaBonus = Annotated[
    HideoutGenericBonus
    | HideoutBonusAdditionalSlots
    | HideoutBonusStashSize
    | HideoutSkillGroupLevelingBonus,
    Discriminator("type"),
]
