from lib.models import CamelCaseModel


class HideoutSettings(CamelCaseModel):
    generator_speed_without_fuel: float
    generator_fuel_flow_rate: float
    air_filter_unit_flow_rate: float
    gpu_boost_rate: float
