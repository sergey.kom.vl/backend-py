from typing import Annotated, Literal

import pydantic
from pydantic import ConfigDict, Discriminator

from lib.models import CamelCaseModel
from lib.tarkov.profiles.models import HideoutAreaType


class HideoutAreaRequirement(CamelCaseModel):
    model_config = ConfigDict(use_enum_values=True)

    type: Literal["Area"]
    area_type: HideoutAreaType
    required_level: int = pydantic.Field(ge=0)


class HideoutTraderLoyaltyRequirement(CamelCaseModel):
    type: Literal["TraderLoyalty"]
    trader_id: str
    loyalty_level: int


class HideoutItemRequirement(CamelCaseModel):
    type: Literal["Item"]
    template_id: str
    count: int = 1
    is_functional: bool
    is_encoded: bool


class HideoutSkillRequirement(CamelCaseModel):
    type: Literal["Skill"]
    skill_name: str
    skill_level: int


class HideoutToolRequirement(CamelCaseModel):
    type: Literal["Tool"]
    template_id: str


class QuestCompleteRequirement(CamelCaseModel):
    type: Literal["QuestComplete"]
    quest_id: str


class ResourceRequirement(CamelCaseModel):
    type: Literal["Resource"]
    resource: int
    template_id: str


AnyHideoutRequirement = Annotated[
    HideoutAreaRequirement
    | HideoutItemRequirement
    | HideoutTraderLoyaltyRequirement
    | HideoutSkillRequirement
    | HideoutToolRequirement
    | QuestCompleteRequirement
    | ResourceRequirement,
    Discriminator("type"),
]
