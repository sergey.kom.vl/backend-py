from typing import Final

from lib.tarkov.quests.models import QuestTemplate


class QuestStorage:
    def __init__(self, quests: dict[str, QuestTemplate]) -> None:
        self.quests: Final = quests
