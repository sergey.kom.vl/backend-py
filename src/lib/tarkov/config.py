from datetime import timedelta
from typing import Generic, Literal, TypeVar

from pydantic import BaseModel

from lib.models import MinMaxValue, StrictModel

T = TypeVar("T")


class RandomValue(BaseModel, Generic[T]):
    values: list[T]
    weights: list[float | int] | None = None


class WeatherConfig(BaseModel):
    acceleration: int
    clouds: MinMaxValue[float]
    wind_speed: MinMaxValue[float]
    wind_direction: RandomValue[int]
    wind_gustiness: MinMaxValue[float]
    rain: RandomValue[int]
    rain_intensity: MinMaxValue[float]
    fog: MinMaxValue[float]
    temp: MinMaxValue[int]
    pressure: MinMaxValue[int]


class FileSystemStorageConfig(StrictModel):
    mode: Literal["immediate", "deferred"]
    deferred_interval: timedelta


class StorageConfig(StrictModel):
    filesystem: FileSystemStorageConfig


class QuestConfig(StrictModel):
    usec_only: list[str]
    bear_only: list[str]
