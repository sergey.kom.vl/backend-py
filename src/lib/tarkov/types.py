from typing import NewType

ItemId = NewType("ItemId", str)
ItemTemplateId = NewType("ItemTemplateId", str)
QuestId = NewType("QuestId", str)
