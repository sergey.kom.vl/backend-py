import collections
from collections.abc import Sequence
from typing import Any, Protocol, final

from result import Err, Ok, Result

from app.core.domain.traders.repositories import TraderRepository
from app.db.models import AccountProfile, TraderPlayerRestriction
from lib.db import DBContext
from lib.errors import Never
from lib.tarkov.inventory import (
    Inventory,
    InventoryOperationError,
    ItemIsNotFoldableError,
    ItemNotFoundError,
    T,
    regenerate_item_ids,
)
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.errors import (
    NoSpaceForItemError,
    NotEnoughItemsForTradeError,
    TraderNotFoundError,
)
from lib.tarkov.items.actions import (
    FoldAction,
    ItemChanges,
    Location,
    MergeAction,
    MoveAction,
    ProfileChanges,
    RemoveAction,
    SchemeItem,
    SellItem,
    SplitAction,
    TradingConfirmAction,
    TradingSellToTraderAction,
    TransferAction,
)
from lib.tarkov.items.models import Item
from lib.tarkov.items.props.base import StackableItemProps
from lib.tarkov.items.props.item import MoneyProps
from lib.tarkov.items.props.mods import StockProps
from lib.tarkov.items.props.weapons import WeaponProps
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.items.upd import Upd
from lib.tarkov.profiles.models import TraderInfo
from lib.tarkov.trading.models import CurrencyIds, TraderBarter
from lib.tarkov.trading.trader import TraderModel, Traders
from lib.tarkov.types import ItemId, ItemTemplateId
from lib.tarkov.utils import generate_mongodb_id


class ActionHandler(Protocol[T]):
    @property
    def action(self) -> type[T]: ...

    async def handle(
        self,
        *,
        action: T,
        context: ActionContext,
    ) -> Result[None, Any]: ...


@final
class MoveHandler(ActionHandler[MoveAction]):
    action = MoveAction

    async def handle(
        self,
        *,
        action: MoveAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (item := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(id=action.item))

        result = context.inventory.move(
            item=item,
            location=action.to,
        )
        if isinstance(result, Err):
            return result

        return Ok(None)


@final
class SplitHandler(ActionHandler[SplitAction]):
    action = SplitAction

    async def handle(
        self,
        *,
        action: SplitAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (item := context.inventory.get(item_id=action.split_item)):
            return Err(ItemNotFoundError(action.split_item))

        result = context.inventory.split(
            item=item,
            new_id=action.new_item,
            count=action.count,
            location=action.container,
        )
        if isinstance(result, Err):
            return result

        context.changes.items.changed.add(item)

        return Ok(None)


@final
class MergeHandler(ActionHandler[MergeAction]):
    action = MergeAction

    async def handle(
        self,
        *,
        action: MergeAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (source := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(action.item))

        if not (target := context.inventory.get(item_id=action.with_)):
            return Err(ItemNotFoundError(action.with_))

        result = context.inventory.merge(
            source=source,
            target=target,
        )
        if isinstance(result, Err):
            return result
        context.changes.items.deleted.add(source)
        context.changes.items.changed.add(target)
        return Ok(None)


@final
class TransferHandler(ActionHandler[TransferAction]):
    action = TransferAction

    async def handle(
        self,
        *,
        action: TransferAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (source := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(action.item))

        if not (target := context.inventory.get(item_id=action.with_)):
            return Err(ItemNotFoundError(action.with_))

        result = context.inventory.transfer(
            source=source,
            target=target,
            count=action.count,
        )
        if isinstance(result, Err):
            return result

        context.changes.items.changed.add(source)
        context.changes.items.changed.add(target)

        return Ok(None)


@final
class RemoveHandler(ActionHandler[RemoveAction]):
    action = RemoveAction

    async def handle(
        self,
        *,
        action: RemoveAction,
        context: ActionContext,
    ) -> Result[None, InventoryOperationError]:
        if not (item := context.inventory.get(item_id=action.item)):
            return Err(ItemNotFoundError(action.item))

        if isinstance(result := context.inventory.remove(item), Err):
            return result  # pragma: no cover

        item, children = result.ok_value
        context.changes.items.deleted.add(item)
        context.changes.items.deleted.update(children)
        return Ok(None)


@final
class FoldHandler(ActionHandler[FoldAction]):
    action = FoldAction

    def __init__(self, item_db: ItemDB) -> None:
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: FoldAction,
        context: ActionContext,
    ) -> Result[None, ItemNotFoundError | ItemIsNotFoldableError]:
        if not (item := context.inventory.get(action.item)):
            return Err(ItemNotFoundError(action.item))
        tpl = self._item_db.get(item.template_id)
        if not isinstance(tpl.props, WeaponProps | StockProps):
            return Err(ItemIsNotFoldableError(id=item.id))

        parent = next(
            (
                item
                for item in context.inventory.iter_parents(item)
                if self._item_db.get(item.template_id).props.merges_with_children
            ),
            item,
        )
        children = tuple(context.inventory.children(item=parent))
        context.inventory.remove(item=parent)

        item.upd.setdefault("Foldable", {"Folded": False})["Folded"] = action.value
        if item.parent_id is None or item.slot_id is None:
            raise Never

        if parent.parent_id is None or parent.slot_id is None:
            raise Never

        context.inventory.add(
            item=parent,
            children=children,
            location=Location(
                id=parent.parent_id,
                container=parent.slot_id,
                location=parent.location,
            ),
        )

        return Ok(None)


@final
class TradingConfirmHandler(ActionHandler[TradingConfirmAction]):
    action = TradingConfirmAction

    def __init__(
        self,
        traders: Traders,
        item_db: ItemDB,
        repository: TraderRepository,
        db_context: DBContext,
    ) -> None:
        self._traders = traders
        self._item_db = item_db
        self._repository = repository
        self._db_context = db_context

    async def handle(
        self,
        *,
        action: TradingConfirmAction,
        context: ActionContext,
    ) -> Result[
        None,
        TraderNotFoundError
        | NotEnoughItemsForTradeError
        | ItemNotFoundError
        | NoSpaceForItemError,
    ]:
        if not (trader := self._traders.get(action.trader_id)):
            return Err(TraderNotFoundError(id=action.trader_id))
        trader_inventory = self._traders.inventory(trader)
        if not (trader_item := trader_inventory.get(item_id=action.item_id)):
            return Err(ItemNotFoundError(id=action.item_id))

        trader_item_children = list(trader_inventory.children(trader_item))
        template = self._item_db.get(trader_item.template_id)
        max_stack_size = (
            template.props.stack_max_size
            if isinstance(template.props, StackableItemProps)
            else 1
        )
        scheme = trader.assort.barter_scheme[action.item_id][action.scheme_id]
        if not self._scheme_items_valid(
            count=action.count,
            scheme=scheme,
            scheme_items=action.scheme_items,
            inventory=context.inventory,
        ):
            return Err(NotEnoughItemsForTradeError())

        remaining = action.count
        while remaining > 0:
            stack_count = min(remaining, max_stack_size)
            remaining -= stack_count
            buy_result = await self._buy_one(
                context=context,
                trader_item=trader_item,
                trader_item_children=trader_item_children,
                count=stack_count,
            )
            if isinstance(buy_result, Err):
                return buy_result  # pragma: no cover

        self._take_required_items(
            scheme_items=action.scheme_items,
            changes=context.changes.items,
            trader=trader,
            trader_info=context.profile.traders_info[trader.base.id],
            inventory=context.inventory,
        )
        await self._update_restrictions(
            account_profile=context.account_profile,
            trader=trader,
            item_id=action.item_id,
            count=action.count,
        )

        return Ok(None)

    def _scheme_items_valid(
        self,
        count: int,
        scheme: Sequence[TraderBarter],
        scheme_items: Sequence[SchemeItem],
        inventory: Inventory,
    ) -> bool:
        required_items: dict[ItemTemplateId, int] = collections.defaultdict(int)
        for barter_item in scheme:
            required_items[barter_item.template_id] += barter_item.count * count

        items = [inventory.get(item.id) for item in scheme_items]
        for item, scheme_item in zip(items, scheme_items, strict=True):
            if item is None:
                return False

            if item.upd.get("StackObjectsCount", 1) < scheme_item.count:
                return False

            required_items[item.template_id] -= scheme_item.count

        return all(count == 0 for count in required_items.values())

    def _take_required_items(  # noqa: PLR0913
        self,
        scheme_items: Sequence[SchemeItem],
        changes: ItemChanges,
        trader: TraderModel,
        trader_info: TraderInfo,
        inventory: Inventory,
    ) -> None:
        for scheme_item in scheme_items:
            item = inventory.get(scheme_item.id)
            if item is None:
                raise Never
            item_count = item.upd.get("StackObjectsCount", 1) - scheme_item.count

            item_template = self._item_db.get(item.template_id)
            if (
                isinstance(item_template.props, MoneyProps)
                and item_template.props.type == trader.base.currency
            ):
                trader_info.sales_sum += scheme_item.count

            if item_count <= 0:
                inventory.remove(item)
                changes.deleted.add(item)
            else:
                item.upd["StackObjectsCount"] = item_count
                changes.changed.add(item)

    async def _buy_one(
        self,
        context: ActionContext,
        trader_item: Item,
        trader_item_children: Sequence[Item],
        count: int,
    ) -> Result[None, NoSpaceForItemError]:
        item = trader_item.model_copy(deep=True)
        item.parent_id = None
        item.slot_id = None
        item.location = None
        item.upd = Upd(StackObjectsCount=count)

        children = [i.model_copy(deep=True) for i in trader_item_children]
        regenerate_item_ids(item, *children)

        if (stash := context.inventory.get(context.profile.inventory.stash)) is None:
            raise Never

        location = context.inventory.find_location(
            item=item,
            children=children,
            parent=stash,
            slot_id="hideout",
        )
        if location is None:
            return Err(NoSpaceForItemError())  # pragma: no cover
        context.inventory.add(
            item,
            children=children,
            location=location,
        )
        context.changes.items.new.add(item)
        context.changes.items.new.update(children)
        return Ok(None)

    async def _update_restrictions(
        self,
        account_profile: AccountProfile,
        trader: TraderModel,
        item_id: ItemId,
        count: int,
    ) -> None:
        restriction = await self._repository.restriction(
            trader_id=trader.base.id,
            profile=account_profile,
            item_id=item_id,
        )
        trader_db = await self._repository.get(trader.base.id)
        if not trader_db:
            raise Never

        if restriction is None:  # pragma: no branch
            restriction = TraderPlayerRestriction(
                trader=trader_db,
                profile=account_profile,
                item_id=item_id,
            )
        restriction.bought_count += count
        self._db_context.add(restriction)


@final
class TraderSellItemHandler(ActionHandler[TradingSellToTraderAction]):
    action = TradingSellToTraderAction

    def __init__(
        self,
        traders: Traders,
        item_db: ItemDB,
    ) -> None:
        self._traders = traders
        self._item_db = item_db

    async def handle(
        self,
        *,
        action: TradingSellToTraderAction,
        context: ActionContext,
    ) -> Result[
        None,
        TraderNotFoundError
        | ItemNotFoundError
        | NotEnoughItemsForTradeError
        | NoSpaceForItemError,
    ]:
        if not (trader := self._traders.get(trader_id=action.trader_id)):
            return Err(TraderNotFoundError(id=action.trader_id))

        if isinstance(
            take_result := self._take_required_items(
                items=action.items,
                inventory=context.inventory,
                changes=context.changes,
            ),
            Err,
        ):
            return take_result

        stash = context.inventory.get(context.profile.inventory.stash)
        if stash is None:
            raise Never

        if isinstance(
            money_add_result := self._add_money(
                inventory=context.inventory,
                stash=stash,
                currency_id=CurrencyIds[trader.base.currency].value,
                currency_amount=action.price,
                changes=context.changes,
            ),
            Err,
        ):
            return money_add_result

        return Ok(None)

    def _take_required_items(
        self,
        items: Sequence[SellItem],
        inventory: Inventory,
        changes: ProfileChanges,
    ) -> Result[None, ItemNotFoundError | NotEnoughItemsForTradeError]:
        for sell_item in items:
            item = inventory.get(sell_item.id)
            if item is None:
                return Err(ItemNotFoundError(id=sell_item.id))

            count = item.upd.get("StackObjectsCount", 1)
            count -= sell_item.count
            if count < 0:
                return Err(NotEnoughItemsForTradeError())
            if count == 0:
                inventory.remove(item)
                changes.items.deleted.add(item)
            else:  # pragma: no cover
                item.upd["StackObjectsCount"] = count
                changes.items.changed.add(item)
        return Ok(None)

    def _add_money(  # noqa: PLR0913
        self,
        inventory: Inventory,
        currency_id: ItemTemplateId,
        currency_amount: int,
        changes: ProfileChanges,
        stash: Item,
    ) -> Result[None, NoSpaceForItemError]:
        max_stack_size = self._item_db.get(currency_id).props.stack_max_size

        existing_items = (
            item for item in inventory.items.values() if item.template_id == currency_id
        )

        currency_left = currency_amount
        for item in existing_items:
            count = item.upd.get("StackObjectsCount", 1)
            if count >= max_stack_size:
                continue
            diff = min(max_stack_size - count, currency_left)
            currency_left -= diff
            item.upd["StackObjectsCount"] = count + diff
            changes.items.changed.add(item)

        while currency_left > 0:
            count = min(max_stack_size, currency_left)
            currency_left -= count
            money = Item(
                id=generate_mongodb_id(),
                template_id=currency_id,
                upd=Upd(StackObjectsCount=count),
            )
            location = inventory.find_location(
                item=money,
                children=(),
                parent=stash,
                slot_id="hideout",
            )
            if location is None:
                return Err(NoSpaceForItemError())
            inventory.add(
                money,
                children=(),
                location=location,
            )
            changes.items.new.add(money)
        return Ok(None)
