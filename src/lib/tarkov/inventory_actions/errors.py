import dataclasses


@dataclasses.dataclass
class TraderNotFoundError:
    id: str


@dataclasses.dataclass
class NotEnoughItemsForTradeError:
    pass


@dataclasses.dataclass
class NoSpaceForItemError:
    pass
