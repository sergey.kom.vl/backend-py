import logging
from collections.abc import Mapping, Sequence
from typing import Any, Protocol, Self

from app.core.domain.traders.repositories import TraderRepository
from app.db.models import AccountProfile
from lib.db import DBContext
from lib.tarkov.inventory import Inventory, T
from lib.tarkov.inventory_actions import ActionContext
from lib.tarkov.inventory_actions.actions import (
    ActionHandler,
    FoldHandler,
    MergeHandler,
    MoveHandler,
    RemoveHandler,
    SplitHandler,
    TraderSellItemHandler,
    TradingConfirmHandler,
    TransferHandler,
)
from lib.tarkov.items.actions import AnyAction, ItemsMovingResult, ProfileChanges
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.models import Profile
from lib.tarkov.trading.trader import Traders


class ActionHandlersDict(Protocol):
    def __getitem__(self, item: type[T]) -> ActionHandler[T]: ...

    @classmethod
    def from_mapping(
        cls,
        obj: Mapping[type[AnyAction], ActionHandler[AnyAction]],
    ) -> Self:
        return obj  # type: ignore[return-value]


class ActionRouter:
    def __init__(
        self,
        item_db: ItemDB,
        traders: Traders,
        db_context: DBContext,
        trader_repository: TraderRepository,
    ) -> None:
        self._item_db = item_db
        self._traders = traders
        self._db_context = db_context
        self._trader_repository = trader_repository

    async def handle(
        self,
        account_profile: AccountProfile,
        profile: Profile,
        inventory: Inventory,
        actions: Sequence[AnyAction],
    ) -> ItemsMovingResult:
        handlers: Sequence[ActionHandler[Any]] = (
            MoveHandler(),
            SplitHandler(),
            MergeHandler(),
            TransferHandler(),
            RemoveHandler(),
            FoldHandler(item_db=self._item_db),
            TradingConfirmHandler(
                traders=self._traders,
                item_db=self._item_db,
                db_context=self._db_context,
                repository=self._trader_repository,
            ),
            TraderSellItemHandler(traders=self._traders, item_db=self._item_db),
        )
        action_handlers = ActionHandlersDict.from_mapping(
            {handler.action: handler for handler in handlers},
        )
        logging.debug(actions)
        context = ActionContext(
            account_profile=account_profile,
            profile=profile,
            changes=ProfileChanges(
                id=profile.id,
                health=profile.health,
                skills=profile.skills,
            ),
            inventory=inventory,
        )

        for action in actions:
            handler = action_handlers[type(action)]
            result = await handler.handle(action=action, context=context)
            result.unwrap()

        profile.inventory.items = inventory.to_list()
        context.changes.experience = profile.info.experience
        context.changes.trader_relations = profile.traders_info

        return ItemsMovingResult(
            changes={profile.id: context.changes},
            warnings=[],
        )
