from pathlib import Path

asset_dir_path = Path("assets")
bots_dir_path = asset_dir_path.joinpath("bot", "bots")
editions_dir_path = asset_dir_path.joinpath("editions")
static_files_dir = asset_dir_path.joinpath("static")

achievements_path = asset_dir_path.joinpath("achievements.json")
