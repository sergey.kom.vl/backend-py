import enum


class ItemRarity(enum.StrEnum):
    common = "Common"
    rare = "Rare"
    superrare = "Superrare"


Color = str
