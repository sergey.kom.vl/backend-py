import enum
import logging
from typing import TYPE_CHECKING, Annotated, Any, Literal, TypeVar

import pydantic
from pydantic import BeforeValidator, ConfigDict, model_validator

from lib.models import CoerceEnumValues, StrictModel
from lib.tarkov._node import NodeBase
from lib.tarkov.items.props import AnyProps, props_map
from lib.tarkov.items.upd import Upd
from lib.tarkov.types import ItemId, ItemTemplateId

T = TypeVar("T")


class ItemNode(NodeBase):
    type: Literal["Node"]
    props: dict[str, Any]


class ItemTemplate(NodeBase):
    model_config = ConfigDict(json_schema_extra={})

    type: Literal["Item"]

    if TYPE_CHECKING:
        props: AnyProps
    else:
        props: Any

    @model_validator(mode="before")
    def _validate_props(cls, data: dict[str, Any]) -> dict[str, Any]:
        if (parent := data.get("_parent")) not in props_map:  # pragma: no cover
            logging.warning(
                "Node id %s not found in props map, this is normal during openapi schema generation",
                parent,
            )
            return data
        props_model = props_map[data["_parent"]]
        data["_props"] = props_model.model_validate(data["_props"], strict=True)
        return data


class ItemRotation(enum.Enum):
    Horizontal = 0
    Vertical = 1


class ItemLocation(StrictModel):
    x: int
    y: int
    rotation: Annotated[
        ItemRotation,
        BeforeValidator(CoerceEnumValues(ItemRotation)),
        pydantic.Field(alias="r"),
    ]
    is_searched: bool | None = pydantic.Field(None, alias="isSearched")


class Item(StrictModel):
    id: ItemId = pydantic.Field(alias="_id")
    parent_id: ItemId | None = pydantic.Field(None, alias="parentId")
    template_id: ItemTemplateId = pydantic.Field(alias="_tpl")
    upd: Upd = pydantic.Field(default_factory=lambda: Upd())
    slot_id: str | None = pydantic.Field(None, alias="slotId")
    location: ItemLocation | int | None = None

    def __hash__(self) -> int:
        return hash(self.id)
