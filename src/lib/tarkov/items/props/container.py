from typing import ClassVar

import pydantic

from lib.tarkov.items.props.base import CompoundItemProps, SearchableItemProps
from lib.tarkov.items.props.types import ContainerGrid
from lib.tarkov.types import ItemTemplateId


class StashProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("566abbb64bdc2d144c8b457d")


class LockableContainerProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5671435f4bdc2d96058b4569")
    key_ids: list[str]
    tag_color: int
    tag_name: str


class StationaryContainerProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("567583764bdc2d98058b456e")


class MobContainerProps(SearchableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448bf274bdc2dfc2f8b456a")

    contain_type: list[str] = pydantic.Field(alias="containType")
    is_secured: bool = pydantic.Field(alias="isSecured")
    loot_filter: list[str] = pydantic.Field(alias="lootFilter")
    max_count_spawn: int = pydantic.Field(alias="maxCountSpawn")
    min_count_spawn: int = pydantic.Field(alias="minCountSpawn")
    opened_by_key_id: list[str] = pydantic.Field(alias="openedByKeyID")
    size_height: int = pydantic.Field(alias="sizeHeight")
    size_width: int = pydantic.Field(alias="sizeWidth")
    spawn_rarity: str = pydantic.Field(alias="spawnRarity")
    spawn_types: str = pydantic.Field(alias="spawnTypes")
    grids: list[ContainerGrid]


class PocketProps(SearchableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("557596e64bdc2dc2118b4571")


class InventoryProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55d720f24bdc2d88028b456d")


class LockableContainer(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5671435f4bdc2d96058b4569")

    key_ids: list[str]
    tag_color: int
    tag_name: str


class SimpleContainerProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5795f317245977243854e041")

    tag_color: int
    tag_name: str


class LootContainerProps(SearchableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("566965d44bdc2d814c8b4571")

    # I don't see it being used anywhere
    spawn_filter: list[None]


class SortingTableProps(StashProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("6050cac987d3f925bf016837")


class RandomLootContainer(SearchableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("62f109593b54472778797866")

    # Doesn't seem to be present on any of the items
    random_loot_settings: object | None = None


class HideoutAndContainerProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("63da6da4784a55176c018dba")

    layout_name: str
