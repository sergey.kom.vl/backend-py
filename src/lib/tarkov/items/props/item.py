from typing import ClassVar

import pydantic

from lib.models import Vector
from lib.tarkov.items.props.base import BaseProps, StackableItemProps
from lib.tarkov.items.props.types import EffectsMixin, StackSlot
from lib.tarkov.types import ItemTemplateId


class MedsProps(BaseProps, EffectsMixin):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be5664bdc2dd4348b4569")

    max_hp_resource: float
    stimulator_buffs: str
    hp_resource_rate: float = pydantic.Field(alias="hpResourceRate")
    med_effect_type: str = pydantic.Field(alias="medEffectType")
    med_use_time: float = pydantic.Field(alias="medUseTime")


class MedicalProps(MedsProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448f3ac4bdc2dce718b4569")


class DrugsProps(MedsProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448f3a14bdc2d27728b4569")


class MedKitProps(MedsProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448f39d4bdc2d0a728b4568")


class KeyProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be5e94bdc2df1348b4568")

    maximum_number_of_usage: int


class KeyMechanicalProps(KeyProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5c99f98d86f7745c314214b3")


class FoodDrinkProps(BaseProps, EffectsMixin):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be6674bdc2df1348b4569")

    max_resource: float
    stimulator_buffs: str
    food_effect_type: str = pydantic.Field(alias="foodEffectType")
    food_use_time: float = pydantic.Field(alias="foodUseTime")


class DrinkProps(FoodDrinkProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448e8d64bdc2dce718b4568")


class FoodProps(FoodDrinkProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448e8d04bdc2ddf718b4569")


class SpecItemProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447e0e74bdc2d3c308b4567")

    ap_resource: float = pydantic.Field(alias="apResource")
    kr_resource: float = pydantic.Field(alias="krResource")


class CultistAmuletProps(SpecItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("64b69b0c8f3be32ed22682f8")

    max_usages: int


class MoneyProps(StackableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be5dd4bdc2deb348b4569")

    eq_max: int = pydantic.Field(alias="eqMax")
    eq_min: int = pydantic.Field(alias="eqMin")
    rate: int = pydantic.Field(alias="rate")
    type: str = pydantic.Field(alias="type")


class AmmoBoxProps(StackableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be5cb4bdc2deb348b4568")

    stack_slots: list[StackSlot]
    ammo_caliber: str = pydantic.Field(alias="ammoCaliber")


class MapProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("567849dd4bdc2d150f8b456e")

    config_path_str: str
    max_markers_count: int
    scale_max: float = pydantic.Field(alias="scaleMax")
    scale_min: float = pydantic.Field(alias="scaleMin")


class AmmoProps(StackableItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5485a8684bdc2da71d8b4567")

    ammo_life_time_sec: int
    armor_damage: int
    armor_distance_distance_damage: Vector[float]
    ballistic_coeficient: float
    blindness: Vector[float]
    bullet_diameter_milimeters: float
    bullet_mass_gram: float
    caliber: str
    contusion: Vector[float]
    damage: int
    deterioration: float
    durability_burn_modificator: float
    explosion_strength: int
    explosion_type: str
    fragment_type: str
    fragmentation_chance: float
    fragments_count: int
    fuze_arm_time_sec: float
    has_grenader_component: bool
    heat_factor: float
    heavy_bleeding_delta: float
    initial_speed: int
    is_light_and_sound_shot: bool
    light_and_sound_shot_angle: int
    light_and_sound_shot_self_contusion_strength: float
    light_and_sound_shot_self_contusion_time: int
    light_bleeding_delta: float
    malf_feed_chance: float
    malf_misfire_chance: float
    max_explosion_distance: float
    max_fragments_count: int
    min_explosion_distance: int
    min_fragments_count: int
    misfire_chance: float
    penetration_chance: float
    penetration_power: int
    penetration_power_diviation: float
    projectile_count: int
    remove_shell_after_fire: bool
    ricochet_chance: float
    show_bullet: bool
    show_hit_effect_on_explode: bool
    speed_retardation: float
    stamina_burn_per_damage: float
    tracer: bool
    tracer_color: str
    tracer_distance: float

    ammo_accr: int = pydantic.Field(alias="ammoAccr")
    ammo_dist: int = pydantic.Field(alias="ammoDist")
    ammo_hear: int = pydantic.Field(alias="ammoHear")
    ammo_rec: int = pydantic.Field(alias="ammoRec")
    ammo_sfx: str = pydantic.Field(alias="ammoSfx")
    ammo_shift_chance: int = pydantic.Field(alias="ammoShiftChance")
    ammo_type: str = pydantic.Field(alias="ammoType")
    buckshot_bullets: int = pydantic.Field(alias="buckshotBullets")
    casing_eject_power: int = pydantic.Field(alias="casingEjectPower")
    casing_mass: float = pydantic.Field(alias="casingMass")
    casing_name: str = pydantic.Field(alias="casingName")
    casing_sounds: str = pydantic.Field(alias="casingSounds")


class InfoProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448ecbe4bdc2d60728b4568")


class RepairKitProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("616eb7aea207f41933308f46")

    max_repair_resource: int
    repair_quality: int
    repair_strategy_types: list[str]
    repair_type: str
    target_item_filter: list[str]
    ap_resource: float = pydantic.Field(alias="apResource")
    kr_resource: float = pydantic.Field(alias="krResource")


class StimulatorProps(MedsProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448f3a64bdc2d60728b456a")


class KeycardProps(KeyProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5c164d2286f774194c5e69fa")


class CompassProps(SpecItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5f4fbaaca5573a5ac31db429")


class PortableRangefinderProps(SpecItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("61605ddea09d851a0a0c1bbc")

    max_optic_zoom: float


class RadioTransmitterProps(SpecItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("62e9103049c018f425059f38")

    is_encoded: bool
