from typing import ClassVar

import pydantic

from lib.models import RGBAColor
from lib.tarkov.items.props.base import CompoundItemProps
from lib.tarkov.items.props.types import Slot, StackSlot
from lib.tarkov.types import ItemTemplateId


class ModProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448fe124bdc2da5018b4567")

    accuracy: float
    blocks_collapsible: bool
    blocks_folding: bool
    double_action_accuracy_penalty_mult: float
    durability: float
    effective_distance: int
    ergonomics: float
    has_shoulder_contact: bool
    is_animated: bool
    loudness: float
    raid_moddable: bool
    recoil: float
    sighting_range: int
    tool_moddable: bool
    unique_animation_mod_id: int = pydantic.Field(alias="UniqueAnimationModID")
    velocity: float


class MasterModProps(ModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55802f4a4bdc2ddb688b4569")


class BarrelProps(MasterModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("555ef6e44bdc2de9068b457e")

    center_of_impact: float
    cool_factor: float
    deviation_curve: float
    deviation_max: float
    durability_burn_modificator: float
    heat_factor: float
    is_silencer: bool
    shotgun_dispersion: float


class PistolGripProps(MasterModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818a684bdc2ddd698b456d")


class ReceiverProps(MasterModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818a304bdc2db5418b457d")

    cool_factor: float
    durability_burn_modificator: float
    heat_factor: float


class HandguardProps(MasterModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818a104bdc2db9688b4569")

    cool_factor: float
    heat_factor: float


class GearModProps(ModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55802f3e4bdc2de7118b4584")


class MagazineProps(GearModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448bc234bdc2d3c308b4569")

    belt_magazine_refresh_count: int
    can_admin: bool
    can_fast: bool
    can_hit: bool
    cartridges: list[StackSlot]
    check_override: int
    check_time_modifier: float
    is_magazine_for_stationary_weapon: bool
    load_unload_modifier: int
    magazine_with_belt: bool
    malfunction_chance: float
    reload_mag_type: str
    tag_color: int
    tag_name: str
    visible_ammo_ranges_string: str
    mag_animation_index: int = pydantic.Field(alias="magAnimationIndex")


class ChargeProps(GearModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818a6f4bdc2db9688b456b")

    durability_burn_modificator: float


class MountProps(GearModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818b224bdc2dde698b456f")

    cool_factor: float
    heat_factor: float


class StockProps(GearModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818a594bdc2db9688b456a")

    cool_factor: float
    durability_burn_modificator: float
    foldable: bool
    heat_factor: float
    is_shoulder_contact: bool
    retractable: bool
    size_reduce_right: int


class LauncherProps(GearModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818b014bdc2ddc698b456b")

    linked_weapon: str
    use_ammo_without_shell: bool
    chambers: list[Slot]


class CylinderMagazine(MagazineProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("610720f290b75a49ff2e5e25")


class SpringDrivenCylinder(CylinderMagazine):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("627a137bf21bc425b06ab944")


class FunctionalModProps(ModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("550aa4154bdc2dd8348b456b")


class GasBlockProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("56ea9461d2720b67698b456f")

    cool_factor: float
    durability_burn_modificator: float
    heat_factor: float


class TacticalComboProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818b164bdc2ddc698b456c")

    modes_count: int


class MuzzleProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448fe394bdc2d0d028b456c")

    cool_factor: float
    durability_burn_modificator: float
    heat_factor: float
    muzzle_mod_type: str = pydantic.Field(alias="muzzleModType")


class SilencerProps(MuzzleProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("550aa4cd4bdc2dd8348b456c")


class MuzzleComboProps(MuzzleProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("550aa4dd4bdc2dc9348b4569")


class FlashHiderProps(MuzzleProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("550aa4bf4bdc2dd6348b456b")


class FlashlightProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818b084bdc2d5b648b4571")

    modes_count: int


class BipodProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818afb4bdc2dde698b456d")


class AuxilaryMod(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5a74651486f7744e73386dd1")
    cool_factor: float
    durability_burn_modificator: float
    heat_factor: float
    shifts_aim_camera: float


class ForeGripProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818af64bdc2d5b648b4570")


class SightProps(FunctionalModProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5448fe7a4bdc2d6f028b456b")

    aim_sensitivity: list[list[float]]
    calibration_distances: list[list[int]]
    custom_aim_plane: str
    modes_count: list[int]
    scopes_count: int
    zooms: list[list[float]]
    sight_mod_type: str = pydantic.Field(alias="sightModType")


class CompactCollimatorProps(SightProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818acf4bdc2dde698b456b")


class SpecialScopeProps(SightProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818aeb4bdc2ddc698b456a")


class IronSightProps(SightProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818ac54bdc2d5b648b456e")


class CollimatorProps(SightProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818ad54bdc2ddc698b4569")


class AssaultScopeProps(SightProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818add4bdc2d5b648b456f")


class OpticScope(SightProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("55818ae44bdc2dde698b456c")


class ThermalVisionProps(SpecialScopeProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5d21f59b6dbe99052b54ef83")

    cold_max: float
    depth_fade: float
    heat_min: float
    is_fps_stuck: bool
    is_glitch: bool
    is_motion_blurred: bool
    is_noisy: bool
    is_pixelated: bool
    main_tex_color_coef: float
    mask: str
    mask_size: float
    minimum_temperature_value: float
    noise_intensity: float
    pixelation_block_count: int
    ramp_palette: str
    ramp_shift: float
    roughness_coef: float
    specular_coef: float


class NightVisionProps(SpecialScopeProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5a2c3a9486f774688b05e574")

    color: RGBAColor
    diffuse_intensity: float
    has_hinge: bool
    intensity: float
    mask: str
    mask_size: float
    noise_intensity: float
    noise_scale: int


AnyFunctionalMod = (
    FunctionalModProps
    | GasBlockProps
    | FlashlightProps
    | TacticalComboProps
    | MuzzleProps
    | SilencerProps
    | MuzzleComboProps
    | FlashHiderProps
    | BipodProps
    | AuxilaryMod
    | ForeGripProps
    | SightProps
    | CompactCollimatorProps
    | SpecialScopeProps
    | IronSightProps
    | CollimatorProps
    | AssaultScopeProps
    | OpticScope
    | ThermalVisionProps
    | NightVisionProps
)
AnyMasterMod = (
    MasterModProps | BarrelProps | PistolGripProps | ReceiverProps | HandguardProps
)
AnyGearMod = (
    GearModProps
    | MagazineProps
    | ChargeProps
    | MountProps
    | StockProps
    | LauncherProps
    | CylinderMagazine
    | SpringDrivenCylinder
)
AnyModProps = AnyFunctionalMod | AnyMasterMod | AnyGearMod | ModProps
