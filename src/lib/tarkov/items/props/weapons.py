from typing import Any, ClassVar

import pydantic

from lib.models import Vector
from lib.tarkov.types import ItemTemplateId

from .base import BaseProps, CompoundItemProps
from .types import Slot


class WeaponProps(CompoundItemProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5422acb9af1c889c16000029")

    adjust_collimators_to_trajectory: bool
    aim_plane: float
    aim_sensitivity: float
    allow_feed: bool
    allow_jam: bool
    allow_misfire: bool
    allow_overheat: bool
    allow_slide: bool
    ammo_caliber: str = pydantic.Field(alias="ammoCaliber")
    b_eff_dist: int = pydantic.Field(alias="bEffDist")
    b_firerate: int = pydantic.Field(alias="bFirerate")
    b_hear_dist: int = pydantic.Field(alias="bHearDist")
    base_malfunction_chance: float
    block_left_stance: bool = pydantic.Field(alias="blockLeftStance")
    bolt_action: bool
    burst_shots_count: int
    camera_snap: float
    camera_to_weapon_angle_speed_range: Vector[float]
    camera_to_weapon_angle_step: float
    can_queue_second_shot: bool
    center_of_impact: float
    chamber_ammo_count: int = pydantic.Field(alias="chamberAmmoCount")
    chambers: list[Slot]
    compact_handling: bool
    cool_factor_gun: float
    cool_factor_gun_mods: float
    def_ammo: str = pydantic.Field(alias="defAmmo")
    def_mag_type: str = pydantic.Field(alias="defMagType")
    description: str
    deviation_curve: float
    deviation_max: float
    double_action_accuracy_penalty: float
    durability: float
    durability_burn_ratio: float
    durability_spawn_max: float = pydantic.Field(alias="durabSpawnMin")
    durability_spawn_min: float = pydantic.Field(alias="durabSpawnMax")
    ergonomics: int
    foldable: bool
    folded_slot: str
    heat_factor_by_shot: float
    heat_factor_gun: float
    hip_accuracy_restoration_delay: float
    hip_accuracy_restoration_speed: float
    hip_innaccuracy_gain: float
    iron_sight_range: int
    is_belt_machine_gun: bool
    is_bolt_catch: bool = pydantic.Field(alias="isBoltCatch")
    is_chamber_load: bool = pydantic.Field(alias="isChamberLoad")
    is_fast_reload: bool = pydantic.Field(alias="isFastReload")
    is_flare_gun: bool
    is_grenade_launcher: bool
    is_oneoff: bool
    is_stationary_weapon: bool
    manual_bolt_catch: bool
    max_durability: int
    max_repair_degradation: float
    max_repair_kit_degradation: float
    min_repair_degradation: float
    min_repair_kit_degradation: float
    must_bolt_be_openned_for_external_reload: bool
    must_bolt_be_openned_for_internal_reload: bool
    no_firemode_on_boltcatch: bool
    operating_resource: int
    post_recoil_horizontal_range_hand_rotation: Vector[float]
    post_recoil_vertical_range_hand_rotation: Vector[float]
    progress_recoil_angle_on_stable: Vector[float]
    recoil_angle: int
    recoil_camera: float
    recoil_category_multiplier_hand_rotation: float
    recoil_center: Vector[float]
    recoil_damping_hand_rotation: float
    recoil_dispersion: int = pydantic.Field(alias="RecolDispersion")
    recoil_force_back: int
    recoil_force_up: int
    recoil_pos_z_mult: float
    recoil_return_path_damping_hand_rotation: float
    recoil_return_path_offset_hand_rotation: float
    recoil_return_speed_hand_rotation: float
    recoil_stable_angle_increase_step: float
    recoil_stable_index_shot: int
    reload_mode: str
    repair_complexity: int
    retractable: bool
    rotation_center: Vector[float]
    rotation_center_no_stock: Vector[float]
    shotgun_dispersion_: int = pydantic.Field(alias="shotgunDispersion")
    shots_group_settings: Any
    sighting_range: int
    single_fire_rate: int
    size_reduce_right: int
    tactical_reload_fixation: float
    tactical_reload_stiffnes: Vector[float]
    velocity: float
    weapon_class: str = pydantic.Field(alias="weapClass")
    weapon_fire_type: list[str] = pydantic.Field(alias="weapFireType")
    weapon_use_type: str = pydantic.Field(alias="weapUseType")
    with_animator_aiming: bool


class AssaultRifleProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b5f14bdc2d61278b4567")


class PistolProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b5cf4bdc2d65278b4567")


class ShotgunProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b6094bdc2dc3278b4567")

    shotgun_dispersion: float


class SniperRifleProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b6254bdc2dc3278b4568")


class AssaultCarbineProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b5fc4bdc2d87278b4567")


class MarksmanRifleProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b6194bdc2d67278b4567")


class SMGProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447b5e04bdc2d62278b4567")


class GrenadeLauncherProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447bedf4bdc2d87278b4568")


class RevolverProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("617f1ef5e8b54b0998387733")


class MachineGunProps(WeaponProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447bed64bdc2d97278b4568")


class KnifeProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("5447e1d04bdc2dff2f8b4567")

    additional_animation_layer: int
    applied_head_rotation: Vector[float]
    applied_trunk_rotation: Vector[float]
    collider_scale_multiplier: Vector[float]
    deflection_consumption: float
    display_on_model: bool
    durability: float
    min_repair_degradation: float
    max_repair_degradation: float
    max_durability: float
    max_resource: float
    primary_consumption: float
    primary_distance: float
    secondry_consumption: float
    secondry_distance: float
    slash_penetration: float
    stab_penetration: float
    stamina_burn_rate: float
    stimulator_buffs: str

    knife_durability: float = pydantic.Field(alias="knifeDurab")
    knife_hit_delay: float = pydantic.Field(alias="knifeHitDelay")
    knife_hit_radius: float = pydantic.Field(alias="knifeHitRadius")
    knife_hit_slash_damage: float = pydantic.Field(alias="knifeHitSlashDam")
    knife_hit_slash_rate: float = pydantic.Field(alias="knifeHitSlashRate")
    knife_hit_stab_damage: float = pydantic.Field(alias="knifeHitStabDam")
    knife_hit_stab_rate: float = pydantic.Field(alias="knifeHitStabRate")


class ThrowingWeaponProps(BaseProps):
    id: ClassVar[ItemTemplateId] = ItemTemplateId("543be6564bdc2df4348b4568")

    armor_distance_distance_damage: Vector[float]
    blindness: Vector[float]
    can_be_hidden_during_throw: bool
    contusion: Vector[float]
    contusion_distance: float
    emit_time: int
    expl_delay: float
    explosion_effect_type: str
    fragment_type: str
    fragments_count: int
    max_explosion_distance: float
    min_explosion_distance: float
    min_time_to_contact_explode: float
    strength: float
    throw_type: str
    expl_delay1: float = pydantic.Field(alias="explDelay")
    throw_dam_max: float = pydantic.Field(alias="throwDamMax")


AnyWeaponProps = (
    WeaponProps
    | AssaultRifleProps
    | PistolProps
    | ShotgunProps
    | SniperRifleProps
    | AssaultCarbineProps
    | MarksmanRifleProps
    | SMGProps
    | GrenadeLauncherProps
    | RevolverProps
    | MachineGunProps
    | KnifeProps
    | ThrowingWeaponProps
)
