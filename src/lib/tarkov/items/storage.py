from typing import Final

from lib.tarkov.items.models import ItemNode, ItemTemplate
from lib.tarkov.types import ItemTemplateId


class ItemDB:
    def __init__(
        self,
        items: dict[ItemTemplateId, ItemTemplate],
        nodes: dict[ItemTemplateId, ItemNode],
    ) -> None:
        self.items: Final = items
        self.nodes: Final = nodes

    def get(self, id_: ItemTemplateId) -> ItemTemplate:
        return self.items[id_]

    def get_by_name(self, name: str) -> ItemTemplate:
        return next(  # pragma: no branch
            i for i in self.items.values() if i.name == name
        )
