import dataclasses
from pathlib import Path
from typing import Any, NewType, TypeAlias

from pydantic import TypeAdapter

from lib.tarkov.bots.models import BotConfig
from lib.tarkov.bots.services import read_bot_configs
from lib.utils import read_pydantic_json

AssetsPath = NewType("AssetsPath", Path)

Globals: TypeAlias = dict[str, Any]


@dataclasses.dataclass
class ResourceDB:
    globals_: Globals
    bots: dict[str, BotConfig]


async def create_resource_db(resources_path: AssetsPath) -> ResourceDB:
    return ResourceDB(
        globals_=await read_pydantic_json(
            resources_path.joinpath("core", "globals.json"),
            TypeAdapter(Globals),
        ),
        bots=await read_bot_configs(path=resources_path.joinpath("bot", "bots")),
    )
