import operator
import random
import string
from collections.abc import Callable
from typing import assert_never

from lib.tarkov.quests.models import CompareMethod
from lib.tarkov.types import ItemId

_alphabet = string.digits + "abcdef"
_length = 24


def generate_mongodb_id() -> ItemId:
    return ItemId("".join(random.choices(_alphabet, k=_length)))


def compare_operator(method: CompareMethod) -> Callable[[float, float], bool]:
    if method == ">=":
        return operator.ge
    if method == ">":
        return operator.gt
    if method == "<":
        return operator.lt
    if method == "<=":
        return operator.le

    assert_never(method)
