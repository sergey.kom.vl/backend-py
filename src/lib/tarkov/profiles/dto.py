import dataclasses
from collections.abc import Iterator

from lib.tarkov.inventory import Inventory
from lib.tarkov.profiles.models import Profile, ScavProfile


@dataclasses.dataclass
class ProfileContext:
    pmc: Profile
    scav: ScavProfile
    inventory: Inventory

    def profiles(self) -> Iterator[Profile | ScavProfile]:
        yield self.pmc
        yield self.scav
