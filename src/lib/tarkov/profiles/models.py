from __future__ import annotations

import enum
from pathlib import Path
from typing import Any, Generic, Literal, NewType, TypeVar

import pydantic
from pydantic import ConfigDict, Field

from lib.models import CamelCaseModel, DatetimeInt, PascalCaseModel, StrictModel
from lib.tarkov.bots.models import BodyPart, Health
from lib.tarkov.customization import PMCSide
from lib.tarkov.items.models import Item, ItemTemplate
from lib.tarkov.quests.models import QuestStatus
from lib.tarkov.types import ItemId, QuestId
from lib.utils import snake_to_camel

ProfileStoragePath = NewType("ProfileStoragePath", Path)
ProfileTemplatesPath = NewType("ProfileTemplatesPath", Path)


class HideoutAreaType(enum.IntEnum):
    vents = 0
    security = 1
    lavatory = 2
    stash = 3
    generator = 4
    heating = 5
    water_collector = 6
    med_station = 7
    nutrition_unit = 8
    rest_space = 9
    workbench = 10
    intelligence_center = 11
    shooting_range = 12
    library = 13
    scav_case = 14
    illumination = 15
    place_of_fame = 16
    air_filtering_unit = 17
    solar_power = 18
    booze_generator = 19
    bitcoin_farm = 20
    christmas_tree = 21
    broken_wall = 22
    gym = 23
    weapon_stand = 24
    weapon_stand_secondary = 25


class Quest(CamelCaseModel):
    id: QuestId = pydantic.Field(alias="qid")
    start_time: DatetimeInt
    status: QuestStatus
    status_timers: dict[str, DatetimeInt] = pydantic.Field(default_factory=dict)
    completed_conditions: list[str] = pydantic.Field(default_factory=list)
    available_after: int = 0


class InfoSettings(PascalCaseModel):
    standing_for_kill: float = 0
    aggressor_bonus: float = 0
    role: str | None = None
    bot_difficulty: str | None = None
    experience: int | None = None


TSide = TypeVar("TSide")
TSettings = TypeVar("TSettings")


ProfileStatus = Literal["Free", "Transfer", "MatchWait"]


class _Info(PascalCaseModel, Generic[TSide]):
    model_config = ConfigDict(use_enum_values=True)

    account_type: int
    banned_state: bool
    banned_until: int
    bans: list[str] | None = None
    experience: int
    game_version: str
    has_coop_extension: bool = False
    is_streamer_mode_available: bool
    last_completed_event: dict[str, str] | None = pydantic.Field(
        None,
        alias="lastCompletedEvent",
    )
    last_completed_wipe: dict[str, str] | None = pydantic.Field(
        None,
        alias="lastCompletedWipe",
    )
    last_time_played_as_savage: int
    level: int
    locked_move_commands: bool = Field(alias="lockedMoveCommands")
    lower_nickname: str
    member_category: int
    need_wipe_options: list[Any] = pydantic.Field(default_factory=list)
    nickname: str
    nickname_change_date: int
    registration_date: int
    savage_lock_time: int
    settings: InfoSettings
    side: TSide
    squad_invite_restriction: bool
    voice: str


class PMCInfo(_Info[PMCSide]):
    pass


class ScavInfo(_Info[Literal["Savage"]]):
    pass


class ProfileCustomization(PascalCaseModel):
    head: str
    body: str
    feet: str
    hands: str


class Inventory(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    equipment: ItemId
    fast_panel: dict[str, str]
    quest_raid_items: ItemId
    quest_stash_items: ItemId
    stash: ItemId
    sorting_table: ItemId
    items: list[Item]
    hideout_area_stashes: dict[str, Any]


class HideoutProduction(PascalCaseModel):
    progress: int
    in_progress: bool = pydantic.Field(alias="inProgress")
    recipe_id: str
    start_timestamp: int
    production_time: float
    products: list[object]
    given_items_in_start: list[object]
    interrupted: bool
    need_fuel_for_all_production_time: bool
    need_fuel_for_all_production_time1: bool = pydantic.Field(
        alias="needFuelForAllProductionTime",
    )
    skip_time: int


class HideoutAreaSlot(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    location_index: int
    item: ItemTemplate


class HideoutArea(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel, use_enum_values=True)

    active: bool
    level: int
    complete_time: int
    constructing: bool
    last_recipe: str
    passive_bonuses_enabled: bool
    slots: list[HideoutAreaSlot]
    type_: HideoutAreaType = pydantic.Field(alias="type")


class HideoutImprovement(StrictModel):
    completed: bool
    completed_at: int = pydantic.Field(alias="improveCompleteTimestamp")


class Hideout(PascalCaseModel):
    areas: list[HideoutArea]
    improvement: dict[str, HideoutImprovement]
    production: dict[str, HideoutProduction]
    seed: int = 0


class InsuredItem(StrictModel):
    trader_id: str = pydantic.Field(alias="tid")
    item_id: str = pydantic.Field(alias="itemId")


class OfferUser(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    id: str
    member_type: int
    nickname: str
    rating: float
    is_rating_growing: bool
    avatar: str


class OfferRequirement(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    template_id: str = pydantic.Field(alias="_tpl")
    count: int
    only_functional: bool


class Offer(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    id: str = pydantic.Field(alias="_id")
    int_id: int
    user: OfferUser
    root: str
    items: list[Item]
    requirements: list[OfferRequirement]
    requirements_cost: int
    items_cost: int
    summary_cost: int
    start_time: int
    end_time: int
    loyalty_level: int
    sell_in_one_piece: bool
    priority: bool
    locked: bool
    unlimited_count: bool
    not_available: bool
    current_items_count: int = pydantic.Field(alias="CurrentItemCount")
    sell_result: list[object]


class RagFairInfo(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    rating: float
    is_rating_growing: bool
    offers: list[Offer]


class Skill(PascalCaseModel):
    id: str
    progress: float
    points_earned_during_session: float
    last_access: int


class WeaponMastering(PascalCaseModel):
    id: str
    progress: int


class Skills(PascalCaseModel):
    common: list[Skill]
    mastering: list[WeaponMastering]
    points: int
    bonuses: None = None


class BodyParts(PascalCaseModel):
    head: BodyPart
    chest: BodyPart
    stomach: BodyPart
    left_arm: BodyPart
    right_arm: BodyPart
    left_leg: BodyPart
    right_leg: BodyPart


class ProfileHealth(PascalCaseModel):
    hydration: Health
    energy: Health
    temperature: Health
    body_parts: BodyParts
    update_time: DatetimeInt


class UnlockedInfo(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    unlocked_production_recipe: list[str]


class ConditionCounters(PascalCaseModel):
    counters: list[str]


class TraderInfo(CamelCaseModel):
    disabled: bool
    sales_sum: int = 0
    standing: float
    unlocked: bool


class Bonus(StrictModel):
    model_config = ConfigDict(alias_generator=snake_to_camel)

    id: str
    type_: str = pydantic.Field(alias="type")
    template_id: str


Stats = dict[str, Any]


class TaskConditionCounter(CamelCaseModel):
    id: str
    type: str
    value: int
    source_id: str


TaskConditionCounters = dict[str, TaskConditionCounter]


class Profile(PascalCaseModel):
    id: str = Field(alias="_id")
    account_id: int = Field(alias="aid")
    savage: str = Field(alias="savage")
    info: PMCInfo
    customization: ProfileCustomization
    health: ProfileHealth
    inventory: Inventory
    skills: Skills
    stats: Stats
    encyclopedia: dict[str, bool]
    task_condition_counters: TaskConditionCounters
    insured_items: list[InsuredItem]
    hideout: Hideout
    bonuses: list[Bonus]
    notes: dict[str, list[str]]
    quests: list[Quest]
    traders_info: dict[str, TraderInfo]
    ragfair_info: RagFairInfo
    wish_list: list[str]
    unlocked_info: UnlockedInfo


class ScavProfile(PascalCaseModel):
    id: str = Field(alias="_id")
    account_id: int = Field(alias="aid")
    savage: None = Field(alias="savage")
    info: ScavInfo
    customization: ProfileCustomization
    health: ProfileHealth
    inventory: Inventory
    stats: Stats
    skills: Skills
    encyclopedia: dict[str, bool]
    insured_items: list[InsuredItem]
    hideout: None
    quests: list[Quest]
    ragfair_info: RagFairInfo
    bonuses: list[Bonus]
    traders_info: list[None]
    task_condition_counters: TaskConditionCounters
    unlocked_info: UnlockedInfo
    notes: dict[str, list[str]]
    wish_list: list[str]


class ProfileStorageModel(PascalCaseModel):
    pmc: Profile
    scav: ScavProfile
