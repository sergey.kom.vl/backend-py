import asyncio
import collections
import contextlib
import dataclasses
import logging
from collections.abc import AsyncIterator
from contextlib import AbstractAsyncContextManager
from typing import Final, Self, final

import aiofiles

from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import AccountProfile
from lib.plugins.registry import PluginRegistry
from lib.tarkov.config import FileSystemStorageConfig
from lib.tarkov.inventory import Inventory
from lib.tarkov.items.storage import ItemDB
from lib.tarkov.profiles.abc import ProfileStorage
from lib.tarkov.profiles.dto import ProfileContext
from lib.tarkov.profiles.init import ProfileInitializer
from lib.tarkov.profiles.models import (
    ProfileStorageModel,
    ProfileStoragePath,
)
from lib.utils import read_pydantic_json, safe_rename


@dataclasses.dataclass(slots=True, kw_only=True)
class _ProfileEntry:
    dirty: bool = False
    profile_id: str
    context: ProfileContext
    snapshot: str | None = None

    def make_snapshot(self) -> None:  # pragma: no cover
        self.snapshot = ProfileStorageModel(
            pmc=self.context.pmc,
            scav=self.context.scav,
        ).model_dump_json(indent=4, by_alias=True)


@final
class FileSystemProfileStorage(ProfileStorage):
    _profile_filename: Final[str] = "character.json"

    def __init__(  # noqa: PLR0913
        self,
        storage_path: ProfileStoragePath,
        initializer: ProfileInitializer,
        item_db: ItemDB,
        settings: FileSystemStorageConfig,
        plugins: PluginRegistry,
    ) -> None:
        self._storage_path = storage_path
        self._initializer = initializer
        self._item_db = item_db
        self._settings = settings
        self._plugins = plugins

        self._closed = asyncio.Event()
        self._profiles: dict[str, _ProfileEntry] = {}
        self._locks: dict[str, asyncio.Lock] = collections.defaultdict(asyncio.Lock)
        self._tg = asyncio.TaskGroup()

    @contextlib.asynccontextmanager
    async def profile(
        self,
        profile_id: str,
    ) -> AsyncIterator[ProfileContext]:
        async with self._lock(profile_id=profile_id):
            entry = await self._read(profile_id=profile_id)
            try:
                yield entry.context
                entry.dirty = True
                if self._settings.mode == "deferred":  # pragma: no cover
                    entry.make_snapshot()
            except:
                if self._settings.mode == "deferred":  # pragma: no cover
                    await self._save_snapshot(entry=entry)
                await self._evict(profile_id)
                raise
            else:
                self._tg.create_task(
                    self._save(context=entry.context, profile_id=profile_id),
                )

    async def readonly(self, profile_id: str) -> ProfileContext:
        return (await self._read(profile_id=profile_id)).context

    @contextlib.asynccontextmanager
    async def context(self) -> AsyncIterator[Self]:
        async with self._tg:
            if self._settings.mode == "deferred":  # pragma: no cover
                self._tg.create_task(self._save_worker())
            yield self
            self._closed.set()

    async def initialize_profile(
        self,
        profile: AccountProfile,
        dto: ProfileCreateDTO,
    ) -> None:
        profile_id = str(profile.id)
        async with self._lock(profile_id):
            profile_model = await self._initializer.init(
                profile=profile,
                dto=dto,
            )
            context = ProfileContext(
                pmc=profile_model.pmc,
                scav=profile_model.scav,
                inventory=Inventory.from_sequence(
                    profile_model.pmc.inventory.items,
                    templates=self._item_db.items,
                ),
            )
            self._profiles[profile_id] = _ProfileEntry(
                profile_id=profile_id,
                context=context,
            )
            await self._save(
                context=context,
                profile_id=profile_id,
                lock=False,
            )

    def _lock(self, profile_id: str) -> AbstractAsyncContextManager[None]:
        return self._locks[profile_id]

    async def _evict(self, profile_id: str) -> None:
        del self._profiles[profile_id]
        del self._locks[profile_id]

    async def _save_worker(self) -> None:  # pragma: no cover
        while True:
            await asyncio.wait(
                (
                    asyncio.create_task(
                        asyncio.sleep(self._settings.deferred_interval.total_seconds()),
                    ),
                    asyncio.create_task(self._closed.wait()),
                ),
                return_when=asyncio.FIRST_COMPLETED,
            )
            await self._save_all_profiles()

            if self._closed.is_set():
                return

    async def _save_all_profiles(self) -> None:  # pragma: no cover
        for entry in self._profiles.values():
            if not entry.dirty:
                continue
            await self._save(profile_id=entry.profile_id, context=entry.context)
            entry.dirty = False

    async def _read(self, profile_id: str) -> _ProfileEntry:
        if profile_id not in self._profiles:
            profiles = await read_pydantic_json(
                self._storage_path.joinpath(
                    profile_id,
                    self._profile_filename,
                ),
                ProfileStorageModel,
            )
            context = ProfileContext(
                pmc=profiles.pmc,
                scav=profiles.scav,
                inventory=Inventory.from_sequence(
                    profiles.pmc.inventory.items,
                    templates=self._item_db.items,
                ),
            )
            self._profiles[profile_id] = _ProfileEntry(
                profile_id=profile_id,
                context=context,
            )

        entry = self._profiles[profile_id]
        await self._plugins.profile.on_profile_load(context=entry.context)
        return entry

    async def _save(
        self,
        context: ProfileContext,
        profile_id: str,
        *,
        lock: bool = True,
    ) -> None:
        lock_ctx = (
            self._lock(
                profile_id=profile_id,
            )
            if lock
            else contextlib.nullcontext()
        )

        async with lock_ctx:
            contents = ProfileStorageModel(
                scav=context.scav,
                pmc=context.pmc,
            ).model_dump_json(indent=4, by_alias=True)
            await self._save_impl(
                profile_id=profile_id,
                contents=contents,
            )

    async def _save_snapshot(self, entry: _ProfileEntry) -> None:  # pragma: no cover
        if not entry.snapshot:
            return

        try:
            async with self._lock(entry.profile_id):
                await self._save_impl(
                    profile_id=entry.profile_id,
                    contents=entry.snapshot,
                )
        finally:
            entry.snapshot = None

    async def _save_impl(
        self,
        profile_id: str,
        contents: str,
    ) -> None:
        tmp_path = self._storage_path.joinpath(
            profile_id,
            f"{self._profile_filename}.tmp",
        )
        dest_path = self._storage_path.joinpath(
            profile_id,
            self._profile_filename,
        )

        tmp_path.parent.mkdir(parents=True, exist_ok=True)

        with safe_rename(tmp_path, dest_path):
            logging.info("Saving profile %s", profile_id)
            async with aiofiles.open(tmp_path, "w", encoding="utf8") as f:
                await f.write(contents)
