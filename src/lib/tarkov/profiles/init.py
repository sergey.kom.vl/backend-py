import uuid
from typing import Final

from app.core.domain.profiles.dto import ProfileCreateDTO
from app.db.models import AccountProfile
from lib.models import StrictModel
from lib.tarkov.customization import PMCSide
from lib.tarkov.profiles.models import (
    Profile,
    ProfileStorageModel,
    ProfileTemplatesPath,
    ScavProfile,
    TraderInfo,
)
from lib.tarkov.profiles.utils import regenerate_inventory_ids
from lib.tarkov.trading.trader import Traders
from lib.tarkov.utils import generate_mongodb_id
from lib.utils import read_pydantic_json


class ProfileTemplateCharacters(StrictModel):
    usec: Profile
    bear: Profile


class ProfileTemplate(StrictModel):
    characters: ProfileTemplateCharacters


class ProfileInitializer:
    def __init__(
        self,
        traders: Traders,
        templates_path: ProfileTemplatesPath,
        scav_template: ScavProfile,
    ) -> None:
        self._traders = traders
        self._templates_path = templates_path
        self._scav_template: Final = scav_template

    async def _profile_template(
        self,
        game_edition: str,
        side: PMCSide,
    ) -> ProfileStorageModel:
        path = self._templates_path.joinpath(
            f"{game_edition}.json",
        )
        template = await read_pydantic_json(path, ProfileTemplate)
        pmc = template.characters.usec if side == "Usec" else template.characters.bear
        scav = self._scav_template.model_copy(deep=True)
        return ProfileStorageModel(
            pmc=pmc,
            scav=scav,
        )

    async def init(
        self,
        profile: AccountProfile,
        dto: ProfileCreateDTO,
    ) -> ProfileStorageModel:
        profiles = await self._profile_template(
            game_edition=profile.account.game_edition,
            side=dto.side,
        )
        regenerate_inventory_ids(profiles.pmc.inventory)
        regenerate_inventory_ids(profiles.scav.inventory)

        pmc = profiles.pmc
        pmc.id = generate_mongodb_id()
        pmc.account_id = profile.account_id
        pmc.savage = f"scav{pmc.id}"

        pmc.info.side = dto.side
        pmc.info.nickname = dto.name
        pmc.info.lower_nickname = dto.name.lower()
        pmc.info.voice = dto.voice_id
        pmc.customization.head = dto.head_id
        pmc.traders_info |= {
            trader.base.id: TraderInfo(disabled=False, standing=0, unlocked=True)
            for trader in self._traders
        }

        scav = profiles.scav
        scav.id = pmc.savage
        scav.account_id = profile.account_id
        scav.info.nickname = str(uuid.uuid4())
        scav.info.lower_nickname = scav.info.nickname.lower()
        return profiles
