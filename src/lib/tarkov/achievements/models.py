from typing import Any

from lib.models import CamelCaseModel


class AchievementConditions(CamelCaseModel):
    available_for_finish: list[Any]
    fail: list[Any]


class Achievement(CamelCaseModel):
    id: str
    image_url: str
    asset_path: str
    rewards: list[None]
    conditions: AchievementConditions
    instant_complete: bool
    show_notifications_in_game: bool
    show_progress: bool
    prefab: str
    rarity: str
    hidden: bool
    show_conditions: bool
    progress_bar_enabled: bool
    side: str
    index: int


AchievementsFile = list[Achievement]
