from collections.abc import Mapping
from typing import Any, Literal, TypeAlias

from lib.models import PascalCaseModel

BodyPartType = Literal[
    "Chest",
    "Head",
    "Stomach",
    "LeftArm",
    "RightArm",
    "LeftLeg",
    "RightLeg",
]


class Health(PascalCaseModel):
    current: float
    maximum: float


class BodyPart(PascalCaseModel):
    health: Health


class BotHealth(PascalCaseModel):
    body_parts: Mapping[BodyPartType, BodyPart]


BotDifficultyKey = Literal["easy", "normal", "hard", "impossible"]

BotDifficulty: TypeAlias = dict[str, Any]


class BotConfig(PascalCaseModel):
    name: str
    difficulties: dict[BotDifficultyKey, BotDifficulty]
    health: BotHealth
