import asyncio
from pathlib import Path
from typing import cast

from pydantic import TypeAdapter

from lib.tarkov.bots.models import BotConfig, BotDifficulty, BotDifficultyKey, BotHealth
from lib.utils import read_pydantic_json

_bot_difficulty_adapter = TypeAdapter(BotDifficulty)


async def _read_bot_config(bot_dir: Path) -> BotConfig:
    health = await read_pydantic_json(bot_dir.joinpath("health.json"), BotHealth)

    difficulties = {
        cast(BotDifficultyKey, difficulty_path.stem): await read_pydantic_json(
            difficulty_path,
            _bot_difficulty_adapter,
        )
        for difficulty_path in bot_dir.joinpath("difficulties").glob("*.json")
    }
    return BotConfig(
        name=bot_dir.stem,
        health=health,
        difficulties=difficulties,
    )


async def read_bot_configs(path: Path) -> dict[str, BotConfig]:
    tasks = [_read_bot_config(bot_path) for bot_path in path.iterdir()]
    return {config.name: config for config in await asyncio.gather(*tasks)}
