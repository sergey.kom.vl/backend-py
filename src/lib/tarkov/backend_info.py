from collections.abc import Mapping

import pydantic
from pydantic import AnyHttpUrl, BaseModel, ConfigDict

from lib.utils import snake_to_camel, snake_to_pascal


class BackendInfo(BaseModel):
    model_config = ConfigDict(alias_generator=snake_to_pascal, populate_by_name=True)

    lobby: AnyHttpUrl
    trading: AnyHttpUrl
    messaging: AnyHttpUrl
    main: AnyHttpUrl
    rag_fair: AnyHttpUrl


class ClientGameConfig(BaseModel):
    model_config = ConfigDict(alias_generator=snake_to_camel, populate_by_name=True)

    account_id: str = pydantic.Field(alias="aid")
    active_profile_id: str | None
    languages: Mapping[str, str]
    backend: BackendInfo
    utc_time: int
    total_in_game: int

    lang: str = "en"
    taxonomy: int = 6
    nda_free: bool = False
    report_available: bool = False
    twitch_event_member: bool = False
    use_protobuf: bool = False
