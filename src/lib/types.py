from collections.abc import Iterable
from os import PathLike
from typing import Any, TypeAlias

from aioinject import Provider

Providers: TypeAlias = Iterable[Provider[Any]]
PathOrStr = str | PathLike[str]
